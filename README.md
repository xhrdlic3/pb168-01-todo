# PV168 Project: TODO list

A desktop application to help with organizing tasks.

## Team Information

| Seminar Group | Team |
|-------------- | ---- |
| PV168/01      | 1    |

### Members

| Role           | Person               |
|----------------|----------------------|
|Team Lead       | [Matej Dipčár](https://is.muni.cz/auth/osoba/492666) |
|Member          | [Kristýna Fuchsová](https://is.muni.cz/auth/osoba/492905) |
|Member          | [Ondřej Hrdlička](https://is.muni.cz/auth/osoba/514205) |
|Member          | [Patrik Michal Vlček](https://is.muni.cz/auth/osoba/493059) |

### Evaluators

| Role           | Person               |
|----------------|----------------------|
|Customer        | [Mgr. Peter Balčirák](https://is.muni.cz/auth/osoba/422570) |
|Technical Coach | [Ing. Petr Adámek](https://is.muni.cz/auth/osoba/70897) |
