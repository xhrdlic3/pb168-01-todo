package cz.muni.fi.pv168.project.db.taskcategory;

import cz.muni.fi.pv168.project.data.category.Category;
import cz.muni.fi.pv168.project.data.task.Task;
import cz.muni.fi.pv168.project.db.category.CategoryDao;
import cz.muni.fi.pv168.project.db.category.CategoryManager;
import cz.muni.fi.pv168.project.db.categorytime.CategoryTimeDao;
import cz.muni.fi.pv168.project.db.dependency.DependencyDao;
import cz.muni.fi.pv168.project.db.subtask.SubTaskDao;
import cz.muni.fi.pv168.project.db.task.TaskDao;
import cz.muni.fi.pv168.project.db.task.TaskManager;
import org.apache.derby.jdbc.EmbeddedDataSource;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import java.awt.Color;
import java.time.LocalDate;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

class TaskCategoryDaoTest {
    private static EmbeddedDataSource dataSource;
    private TaskCategoryDao taskCategoryDao;
    private TaskDao taskDao;
    private TaskManager taskManager;
    private Task.TaskBuilder taskBuilder;
    private TaskCategoryManager taskCategoryManager;
    private CategoryManager categoryManager;
    private CategoryDao categoryDao;

    @BeforeAll
    static void initTestDataSource() {
        dataSource = new EmbeddedDataSource();
        dataSource.setDatabaseName("memory:todo-test");
        dataSource.setCreateDatabase("create");
    }

    @BeforeEach
    void setupDao() {
        taskDao = new TaskDao(dataSource, Mockito.mock(SubTaskDao.class), Mockito.mock(DependencyDao.class),
                taskCategoryDao, categoryDao, Mockito.mock(CategoryTimeDao.class));
        taskManager = new TaskManager(dataSource);
        taskBuilder = Task.builder("Testing task", 10L, LocalDate.EPOCH);

        categoryDao = new CategoryDao(dataSource, Mockito.mock(CategoryTimeDao.class));
        categoryManager = new CategoryManager(dataSource);

        taskCategoryDao = new TaskCategoryDao(dataSource);
        taskCategoryManager = new TaskCategoryManager(dataSource);
    }

    @AfterEach
    void cleanUp() {
        taskCategoryManager.dropTable();
        categoryManager.dropTable();
        taskManager.dropTable();
    }

    @Test
    void test_deleteAllAssociationsFor_ExistingTask_AssociationsDeleted() {
        var task = taskBuilder.build();
        taskDao.add(task);
        setupCategories(task.getId());
        taskCategoryDao.deleteAllCategoryAssociationsForTask(task.getId());
        Assertions.assertThat(taskCategoryDao.getAllCategoryIdsForTaskId(task.getId())).isEmpty();
    }

    @Test
    void test_getAllIdsFor_ExistingTaskWithAssociations_AssociationsReturned() {
        var task = taskBuilder.build();
        taskDao.add(task);
        var categories = setupCategories(task.getId());
        Assertions.assertThat(taskCategoryDao.getAllCategoryIdsForTaskId(task.getId()))
                .isEqualTo(List.of(categories.stream().map(Category::getId).toArray()));
    }

    @Test
    void test_addAssociationFor_ExistingTask_AssociationAdded() {
        var task = taskBuilder.build();
        taskDao.add(task);
        var categories = new LinkedList<>(Arrays.asList(
                new Category("First category", Color.BLACK),
                new Category("Second category", Color.WHITE)
        ));
        for (var category : categories) {
            categoryDao.add(category);
            taskCategoryDao.addCategoryAssociationFor(task.getId(), category.getId());
        }
        Assertions.assertThat(taskCategoryDao.getAllCategoryIdsForTaskId(task.getId()))
                .isEqualTo(List.of(categories.stream().map(Category::getId).toArray()));
    }

    @Test
    void test_deleteAssociationFor_ExistingTaskWithAssociations_AssociationDeleted() {
        var task = taskBuilder.build();
        taskDao.add(task);
        var categories = setupCategories(task.getId());
        taskCategoryDao.deleteCategoryAssociationFor(task.getId(), categories.get(0).getId());
        categories.remove(0);
        Assertions.assertThat(taskCategoryDao.getAllCategoryIdsForTaskId(task.getId()))
                .isEqualTo(List.of(categories.stream().map(Category::getId).toArray()));
    }

    private List<Category> setupCategories(long taskId) {
        var categories = new LinkedList<>(Arrays.asList(
                new Category("A", Color.BLACK),
                new Category("B", Color.WHITE),
                new Category("C", Color.PINK)
        ));
        for (var category : categories) {
            categoryDao.add(category);
            taskCategoryDao.addCategoryAssociationFor(taskId, category.getId());
        }
        return categories;
    }
}
