package cz.muni.fi.pv168.project.db.categorytime;

import cz.muni.fi.pv168.project.data.category.Category;
import cz.muni.fi.pv168.project.data.category.CategoryTime;
import cz.muni.fi.pv168.project.data.task.Task;
import cz.muni.fi.pv168.project.db.category.CategoryDao;
import cz.muni.fi.pv168.project.db.category.CategoryManager;
import cz.muni.fi.pv168.project.db.dependency.DependencyDao;
import cz.muni.fi.pv168.project.db.subtask.SubTaskDao;
import cz.muni.fi.pv168.project.db.task.TaskDao;
import cz.muni.fi.pv168.project.db.task.TaskManager;
import cz.muni.fi.pv168.project.db.taskcategory.TaskCategoryDao;
import org.apache.derby.jdbc.EmbeddedDataSource;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import java.awt.Color;
import java.time.LocalDate;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

class CategoryTimeDaoTest {

    private static EmbeddedDataSource dataSource;
    private static CategoryManager categoryManager;
    private CategoryTimeDao categoryTimeDao;
    private CategoryTimeManager categoryTimeManager;
    private CategoryDao categoryDao;
    private TaskDao taskDao;
    private TaskManager taskManager;
    private Task.TaskBuilder taskBuilder;

    @BeforeAll
    static void initTestDataSource() {
        dataSource = new EmbeddedDataSource();
        dataSource.setDatabaseName("memory:todo-test");
        dataSource.setCreateDatabase("create");
    }

    @BeforeEach
    void createCategoryTimeDao() {
        categoryDao = new CategoryDao(dataSource, categoryTimeDao);
        categoryManager = new CategoryManager(dataSource);

        categoryTimeDao = new CategoryTimeDao(dataSource);
        categoryTimeManager = new CategoryTimeManager(dataSource);

        taskDao = new TaskDao(dataSource, Mockito.mock(SubTaskDao.class), Mockito.mock(DependencyDao.class),
                Mockito.mock(TaskCategoryDao.class), categoryDao, categoryTimeDao);
        taskManager = new TaskManager(dataSource);
        taskBuilder = Task.builder("Testing task", 10L, LocalDate.EPOCH);
    }

    @AfterEach
    void categoryTimeDaoCleanUp() {
        categoryTimeManager.dropTable();
        categoryManager.dropTable();
        taskManager.dropTable();
    }

    @Test
    void test_getAllEntitiesFor_ExistingTaskWithEntities_ReturnsAllEntities() {
        var category = setupCategory();
        var categoryTimes = setupCategoryTimes(category);
        Assertions.assertThat(categoryTimeDao.getAllCategoryTimesForCategory(category.getId())).isEqualTo(categoryTimes);
    }

    @Test
    void test_getAllEntitiesForTask_ExistingTaskWithEntities_ReturnsAllEntities() {
        var task = taskBuilder.build();
        taskDao.add(task);
        var category = setupCategory();
        var categoryTimes = setupCategoryTimes(category);
        Assertions.assertThat(categoryTimeDao.getAllCategoryTimesForTask(task.getId())).isEqualTo(categoryTimes);
    }

    @Test
    void test_deleteAllEntitiesFor_ExistingCategoryWithEntities_RemovedAllEntities() {
        var category = setupCategory();
        setupCategoryTimes(category);
        categoryTimeDao.deleteAllCategoryTimesForCategory(category.getId());
        Assertions.assertThat(categoryTimeDao.getAllCategoryTimesForCategory(category.getId())).isEmpty();
    }

    @Test
    void test_deleteAllEntitiesForTask_ExistingCategoryWithEntities_RemovedAllEntities() {
        var task = taskBuilder.build();
        taskDao.add(task);
        var category = setupCategory();
        setupCategoryTimes(category);
        categoryTimeDao.deleteAllCategoryTimesForCategory(category.getId());
        Assertions.assertThat(categoryTimeDao.getAllCategoryTimesForTask(task.getId())).isEmpty();
    }

    @Test
    void test_addEntityFor_ExistingCategory_EntitiesAdded() {
        var category = setupCategory();
        var categoryTimes = setupCategoryTimes(category);
        Assertions.assertThat(categoryTimeDao.getAllCategoryTimesForCategory(category.getId())).isEqualTo(categoryTimes);
    }

    @Test
    void test_update_ExistingEntity_EntityUpdated() {
        var category = setupCategory();
        var categoryTimes = setupCategoryTimes(category);
        categoryTimes.get(0).setHoursSpent(999L);
        categoryTimeDao.update(categoryTimes.get(0));
        Assertions.assertThat(categoryTimeDao.getAllCategoryTimesForCategory(category.getId())).isEqualTo(categoryTimes);
    }

    @Test
    void test_delete_ExistingEntity_EntityDeleted() {
        var category = setupCategory();
        var categoryTimes = setupCategoryTimes(category);
        categoryTimeDao.delete(categoryTimes.get(0));
        categoryTimes.remove(0);
        Assertions.assertThat(categoryTimeDao.getAllCategoryTimesForCategory(category.getId())).isEqualTo(categoryTimes);
    }

    @Test
    void test_fetch_ExistingEntity_EntityFetched() {
        var category = setupCategory();
        var categoryTimes = setupCategoryTimes(category);
        var fetchedEntity = categoryTimeDao.findById(categoryTimes.get(0).getId());
        Assertions.assertThat(fetchedEntity).isEqualTo(categoryTimes.get(0));
    }

    private Category setupCategory() {
        var category = new Category("Work", Color.GREEN);
        categoryDao.add(category);
        return category;
    }

    private List<CategoryTime> setupCategoryTimes(Category category) {
        var categoryTimes = new LinkedList<>(Arrays.asList(
                new CategoryTime(LocalDate.EPOCH, 10L, category.getId()),
                new CategoryTime(LocalDate.EPOCH, 5L, category.getId()),
                new CategoryTime(LocalDate.EPOCH, 999L, category.getId())));
        for (var categoryTime : categoryTimes) {
            categoryTimeDao.addCategoryTimeFor(category.getId(), categoryTime);
        }
        return categoryTimes;
    }
}
