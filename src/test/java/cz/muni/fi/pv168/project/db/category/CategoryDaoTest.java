package cz.muni.fi.pv168.project.db.category;

import cz.muni.fi.pv168.project.data.category.Category;
import cz.muni.fi.pv168.project.data.task.Task;
import cz.muni.fi.pv168.project.db.DataAccessException;
import cz.muni.fi.pv168.project.db.categorytime.CategoryTimeDao;
import cz.muni.fi.pv168.project.db.dependency.DependencyDao;
import cz.muni.fi.pv168.project.db.subtask.SubTaskDao;
import cz.muni.fi.pv168.project.db.task.TaskDao;
import cz.muni.fi.pv168.project.db.task.TaskManager;
import cz.muni.fi.pv168.project.db.taskcategory.TaskCategoryDao;
import cz.muni.fi.pv168.project.db.taskcategory.TaskCategoryManager;
import org.apache.derby.jdbc.EmbeddedDataSource;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import java.awt.Color;
import java.time.LocalDate;
import java.util.Arrays;
import java.util.List;

class CategoryDaoTest {

    private static CategoryTimeDao mockCategoryTimeDao;
    private static EmbeddedDataSource dataSource;
    private static CategoryDao categoryDao;
    private static CategoryManager categoryManager;
    private TaskManager taskManager;
    private TaskDao taskDao;
    private TaskCategoryDao taskCategoryDao;
    private TaskCategoryManager taskCategoryManager;

    @BeforeAll
    static void initTestDataSource() {
        mockCategoryTimeDao = Mockito.mock(CategoryTimeDao.class);
        dataSource = new EmbeddedDataSource();
        dataSource.setDatabaseName("memory:todo-test");
        dataSource.setCreateDatabase("create");
    }

    @BeforeEach
    void createCategoryDao() {
        categoryManager = new CategoryManager(dataSource);
        categoryDao = new CategoryDao(dataSource, mockCategoryTimeDao);
        taskDao = new TaskDao(dataSource, Mockito.mock(SubTaskDao.class), Mockito.mock(DependencyDao.class),
                Mockito.mock(TaskCategoryDao.class), categoryDao, Mockito.mock(CategoryTimeDao.class));
        taskManager = new TaskManager(dataSource);
        taskCategoryDao = new TaskCategoryDao(dataSource);
        taskCategoryManager = new TaskCategoryManager(dataSource);
    }

    @AfterEach
    void categoryDaoCleanUp() {
        taskCategoryManager.dropTable();
        categoryManager.dropTable();
        taskManager.dropTable();
    }

    @Test
    void test_add_ValidCategories_SuccessfullyAdded() {
        var categories = createCategories();
        for (var category : categories
        ) {
            categoryDao.add(category);
        }
        Assertions.assertThat(categoryDao.getAll()).isEqualTo(categories);
    }

    @Test
    void test_update_ExistingCategory_SuccessfullyUpdated() {
        var category = new Category("OriginalName", Color.RED);
        categoryDao.add(category);
        var categoryId = category.getId();
        category.setColor(Color.BLACK);
        category.setName("NewName");
        categoryDao.update(category);
        Assertions.assertThat(categoryDao.fetch(categoryId)).isEqualTo(category);
    }

    @Test
    void test_delete_ExistingCategory_SuccessfullyDeleted() {
        var categories = createCategories();
        var categoryToDelete = categories.get(0);
        for (var category : categories
        ) {
            categoryDao.add(category);
        }
        categoryDao.delete(categoryToDelete);
        Assertions.assertThat(categoryDao.getAll()).doesNotContain(categoryToDelete);
    }

    @Test
    void test_delete_ExistingCategoryWithAssociationToTask_ExceptionThrown() {
        var task = Task.builder("Testing task", 10L, LocalDate.EPOCH).build();
        taskDao.add(task);
        var category = new Category("Testing Category", Color.BLACK);
        categoryDao.add(category);
        taskCategoryDao.addCategoryAssociationFor(task.getId(), category.getId());
        org.junit.jupiter.api.Assertions.assertThrows(DataAccessException.class, () -> categoryDao.delete(category));
    }

    @Test
    void test_fetch_ExistingCategory_SuccessfullyFetched() {
        var categories = createCategories();
        var observedCategory = categories.get(0);
        for (var category : categories) {
            categoryDao.add(category);
        }
        Assertions.assertThat(categoryDao.fetch(observedCategory.getId())).isEqualTo(observedCategory);
    }

    private List<Category> createCategories() {
        return Arrays.asList(
                new Category("Work", Color.GREEN),
                new Category("School", Color.YELLOW),
                new Category("Partner", Color.PINK),
                new Category("Project", Color.BLUE));
    }
}
