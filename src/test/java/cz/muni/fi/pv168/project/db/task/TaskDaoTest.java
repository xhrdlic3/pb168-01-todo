package cz.muni.fi.pv168.project.db.task;

import cz.muni.fi.pv168.project.data.category.Category;
import cz.muni.fi.pv168.project.data.task.Task;
import cz.muni.fi.pv168.project.data.task.TaskStatus;
import cz.muni.fi.pv168.project.db.DataAccessException;
import cz.muni.fi.pv168.project.db.category.CategoryDao;
import cz.muni.fi.pv168.project.db.category.CategoryManager;
import cz.muni.fi.pv168.project.db.categorytime.CategoryTimeDao;
import cz.muni.fi.pv168.project.db.dependency.DependencyDao;
import cz.muni.fi.pv168.project.db.dependency.DependencyManager;
import cz.muni.fi.pv168.project.db.subtask.SubTaskDao;
import cz.muni.fi.pv168.project.db.taskcategory.TaskCategoryDao;
import cz.muni.fi.pv168.project.db.taskcategory.TaskCategoryManager;
import org.apache.derby.jdbc.EmbeddedDataSource;
import org.assertj.core.api.AssertionsForClassTypes;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import java.awt.Color;
import java.time.LocalDate;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

class TaskDaoTest {
    private static EmbeddedDataSource dataSource;
    private TaskDao taskDao;
    private TaskManager taskManager;
    private CategoryDao categoryDao;
    private TaskCategoryDao taskCategoryDao;
    private CategoryManager categoryManager;
    private TaskCategoryManager taskCategoryManager;
    private DependencyManager dependencyManager;
    private DependencyDao dependencyDao;

    @BeforeAll
    static void initTestDataSource() {
        dataSource = new EmbeddedDataSource();
        dataSource.setDatabaseName("memory:todo-test");
        dataSource.setCreateDatabase("create");
    }

    @BeforeEach
    void setupDao() {
        categoryDao = new CategoryDao(dataSource, Mockito.mock(CategoryTimeDao.class));
        taskCategoryDao = new TaskCategoryDao(dataSource);
        taskDao = new TaskDao(dataSource, Mockito.mock(SubTaskDao.class), Mockito.mock(DependencyDao.class),
                taskCategoryDao, categoryDao, Mockito.mock(CategoryTimeDao.class));
        dependencyDao = new DependencyDao(dataSource);

        taskManager = new TaskManager(dataSource);
        categoryManager = new CategoryManager(dataSource);
        taskCategoryManager = new TaskCategoryManager(dataSource);
        dependencyManager = new DependencyManager(dataSource);
    }

    @AfterEach
    void cleanUp() {
        dependencyManager.dropTable();
        taskCategoryManager.dropTable();
        categoryManager.dropTable();
        taskManager.dropTable();
    }

    @Test
    void test_add_NewTaskWithoutCategoryAndDependencies_TaskAdded() {
        var tasks = new LinkedList<>(Arrays.asList(
                Task.builder("A", 10L, LocalDate.EPOCH).build(),
                Task.builder("B", 10L, LocalDate.EPOCH).build()
        ));
        for (var task : tasks) {
            taskDao.add(task);
        }
        AssertionsForClassTypes.assertThat(taskDao.getAll()).isEqualTo(tasks);
    }

    @Test
    void test_add_NewTasksWithCategoriesWithoutDependencies_TasksAdded() {
        var tasks = new LinkedList<>(Arrays.asList(
                Task.builder("A", 10L, LocalDate.EPOCH).build(),
                Task.builder("B", 10L, LocalDate.EPOCH).build()
        ));
        for (var task : tasks) {
            taskDao.add(task);
            setupCategories(task.getId());
        }
        AssertionsForClassTypes.assertThat(taskDao.getAll()).isEqualTo(tasks);
    }

    @Test
    void test_add_NewTasksWithCategoriesAndDependencies_TasksAdded() {
        var tasks = new LinkedList<>(Arrays.asList(
                Task.builder("A", 10L, LocalDate.EPOCH).build(),
                Task.builder("B", 10L, LocalDate.EPOCH).build()
        ));
        var dependencyTasks = new LinkedList<Task>();
        for (var task : tasks) {
            taskDao.add(task);
            setupCategories(task.getId());
            dependencyTasks.addAll(setupDependencies(task.getId()));
        }
        var allTasks = new LinkedList<Task>();
        allTasks.addAll(tasks);
        allTasks.addAll(dependencyTasks);
        AssertionsForClassTypes.assertThat(taskDao.getAll()).asList().contains(allTasks.toArray());
        AssertionsForClassTypes.assertThat(allTasks.size()).isEqualTo(taskDao.getAll().size());
    }

    @Test
    void test_getAll_ExistingTasksWithoutCategoryAndDependencies_AllTaskReturned() {
        var tasks = setupTasks();
        AssertionsForClassTypes.assertThat(taskDao.getAll()).isEqualTo(tasks);
    }

    @Test
    void test_fetch_ExistingTaskWithoutCategoryAndDependencies_TaskFetched() {
        var tasks = setupTasks();
        AssertionsForClassTypes.assertThat(taskDao.fetch(tasks.get(0).getId())).isEqualTo(tasks.get(0));
    }

    @Test
    void test_update_ChangedExistingTasksStatus_TaskUpdated() {
        var tasks = setupTasks();
        tasks.get(0).toggleIsCompleted();
        tasks.get(0).setTaskStatus(TaskStatus.FINISHED);
        taskDao.update(tasks.get(0));
        AssertionsForClassTypes.assertThat(taskDao.getAll()).isEqualTo(tasks);
    }

    @Test
    void test_update_ChangedExistingTasksCategory_TaskUpdated() {
        var tasks = setupTasks();
        setupCategories(tasks.get(0).getId());
        taskDao.update(tasks.get(0));
        AssertionsForClassTypes.assertThat(taskDao.getAll()).isEqualTo(tasks);
    }

    @Test
    void test_update_ChangedExistingTasksDependency_TaskUpdated() {
        var tasks = setupTasks();
        dependencyDao.addDependencyAssociationFor(tasks.get(0).getId(), tasks.get(1).getId());
        taskDao.update(tasks.get(0));
        AssertionsForClassTypes.assertThat(taskDao.getAll()).isEqualTo(tasks);
    }

    @Test
    void test_delete_ExistingTask_TaskDeleted() {
        var tasks = setupTasks();
        taskDao.delete(tasks.get(0));
        tasks.remove(0);
        AssertionsForClassTypes.assertThat(taskDao.getAll()).isEqualTo(tasks);
    }

    @Test
    void test_delete_ExistingTaskWithDependencyAssociation_ExceptionThrown() {
        var tasks = setupTasks();
        dependencyDao.addDependencyAssociationFor(tasks.get(1).getId(), tasks.get(0).getId());
        Assertions.assertThrows(DataAccessException.class, () -> taskDao.delete(tasks.get(0)));
    }

    private List<Task> setupTasks() {
        var tasks = new LinkedList<>(Arrays.asList(
                Task.builder("A Task", 10L, LocalDate.EPOCH).build(),
                Task.builder("B Task", 10L, LocalDate.EPOCH).build(),
                Task.builder("C Task", 10L, LocalDate.EPOCH).build(),
                Task.builder("D Task", 10L, LocalDate.EPOCH).build()
        ));
        for (var task : tasks) {
            taskDao.add(task);
        }
        return tasks;
    }

    private void setupCategories(long taskId) {
        var categories = new LinkedList<>(Arrays.asList(
                new Category("A Category", Color.black),
                new Category("B Category", Color.blue)
        ));
        for (var category : categories) {
            categoryDao.add(category);
            taskCategoryDao.addCategoryAssociationFor(taskId, category.getId());
        }
    }

    private List<Task> setupDependencies(long taskId) {
        var tasks = new LinkedList<>(Arrays.asList(
                Task.builder("A Dependency", 10L, LocalDate.EPOCH).build(),
                Task.builder("B Dependency", 10L, LocalDate.EPOCH).build()
        ));
        for (var task : tasks) {
            taskDao.add(task);
            dependencyDao.addDependencyAssociationFor(task.getId(), taskId);
        }
        return tasks;
    }
}
