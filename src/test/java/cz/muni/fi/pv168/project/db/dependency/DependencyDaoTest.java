package cz.muni.fi.pv168.project.db.dependency;

import cz.muni.fi.pv168.project.data.task.Task;
import cz.muni.fi.pv168.project.db.category.CategoryDao;
import cz.muni.fi.pv168.project.db.categorytime.CategoryTimeDao;
import cz.muni.fi.pv168.project.db.subtask.SubTaskDao;
import cz.muni.fi.pv168.project.db.task.TaskDao;
import cz.muni.fi.pv168.project.db.task.TaskManager;
import cz.muni.fi.pv168.project.db.taskcategory.TaskCategoryDao;
import org.apache.derby.jdbc.EmbeddedDataSource;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import java.time.LocalDate;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

class DependencyDaoTest {
    private static EmbeddedDataSource dataSource;
    private DependencyDao dependencyDao;
    private DependencyManager dependencyManager;
    private TaskDao taskDao;
    private Task.TaskBuilder taskBuilder;
    private TaskManager taskManager;

    @BeforeAll
    static void initTestDataSource() {
        dataSource = new EmbeddedDataSource();
        dataSource.setDatabaseName("memory:todo-test");
        dataSource.setCreateDatabase("create");
    }

    @BeforeEach
    void createDependencyDao() {
        taskDao = new TaskDao(dataSource, Mockito.mock(SubTaskDao.class), dependencyDao,
                Mockito.mock(TaskCategoryDao.class), Mockito.mock(CategoryDao.class), Mockito.mock(CategoryTimeDao.class));
        taskManager = new TaskManager(dataSource);
        taskBuilder = Task.builder("Testing task", 10L, LocalDate.EPOCH);

        dependencyDao = new DependencyDao(dataSource);
        dependencyManager = new DependencyManager(dataSource);
    }

    @AfterEach
    void cleanUp() {
        dependencyManager.dropTable();
        taskManager.dropTable();
    }

    @Test
    void test_getAllIdsFor_TaskWithMultipleDependencies_DependenciesReturned() {
        var task = taskBuilder.build();
        taskDao.add(task);
        var dependencyTasks = setupSubTasks(task.getId());
        Assertions.assertThat(dependencyDao.getAllDependencyIdsForTaskId(task.getId()).toArray())
                .isEqualTo(dependencyTasks.stream().map(Task::getId).toArray());
    }

    @Test
    void test_addAssociationFor_TaskWithDependencies_NewDependencyAdded() {
        var task = taskBuilder.build();
        taskDao.add(task);
        var dependencyTasks = setupSubTasks(task.getId());
        var newDependencyTask = Task.builder("Testing task", 55L, LocalDate.EPOCH).build();
        taskDao.add(newDependencyTask);
        dependencyDao.addDependencyAssociationFor(task.getId(), newDependencyTask.getId());
        dependencyTasks.add(newDependencyTask);
        Assertions.assertThat(dependencyDao.getAllDependencyIdsForTaskId(task.getId()).toArray())
                .isEqualTo(dependencyTasks.stream().map(Task::getId).toArray());
    }

    @Test
    void test_deleteAssociationFor_TaskWithMultipleDependencies_OneDependencyDeleted() {
        var task = taskBuilder.build();
        taskDao.add(task);
        var dependencyTasks = setupSubTasks(task.getId());
        dependencyDao.deleteDependencyAssociationFor(task.getId(), dependencyTasks.get(0).getId());
        dependencyTasks.remove(0);
        Assertions.assertThat(dependencyDao.getAllDependencyIdsForTaskId(task.getId()).toArray())
                .isEqualTo(dependencyTasks.stream().map(Task::getId).toArray());
    }

    @Test
    void test_deleteAllAssociationsFor_TaskWithMultipleDependencies_AllDependenciesDeleted() {
        var task = taskBuilder.build();
        taskDao.add(task);
        setupSubTasks(task.getId());
        dependencyDao.deleteAllDependencyAssociationsForTask(task.getId());
        Assertions.assertThat(dependencyDao.getAllDependencyIdsForTaskId(task.getId())).isEmpty();
    }

    private List<Task> setupSubTasks(long taskId) {
        var dependencyTasks = new LinkedList<>(Arrays.asList(
                Task.builder("Testing task", 10L, LocalDate.EPOCH).build(),
                Task.builder("Testing task", 10L, LocalDate.EPOCH).build(),
                Task.builder("Testing task", 10L, LocalDate.EPOCH).build(),
                Task.builder("Testing task", 10L, LocalDate.EPOCH).build()
        ));
        for (var dependencyTask : dependencyTasks) {
            taskDao.add(dependencyTask);
            dependencyDao.addDependencyAssociationFor(taskId, dependencyTask.getId());
        }
        return dependencyTasks;
    }
}
