package cz.muni.fi.pv168.project;

import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;

/**
 * The example unit tests
 */
final class MainTest {

    @Test
    void example() {
        Assertions.assertThat(Integer.MAX_VALUE)
                .as("This test should always pass")
                .isEqualTo(2_147_483_647);
    }
}
