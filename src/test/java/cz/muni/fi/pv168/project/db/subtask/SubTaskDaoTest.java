package cz.muni.fi.pv168.project.db.subtask;

import cz.muni.fi.pv168.project.data.task.SubTask;
import cz.muni.fi.pv168.project.data.task.Task;
import cz.muni.fi.pv168.project.db.category.CategoryDao;
import cz.muni.fi.pv168.project.db.categorytime.CategoryTimeDao;
import cz.muni.fi.pv168.project.db.dependency.DependencyDao;
import cz.muni.fi.pv168.project.db.task.TaskDao;
import cz.muni.fi.pv168.project.db.task.TaskManager;
import cz.muni.fi.pv168.project.db.taskcategory.TaskCategoryDao;
import org.apache.derby.jdbc.EmbeddedDataSource;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import java.time.LocalDate;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

class SubTaskDaoTest {
    private static EmbeddedDataSource dataSource;
    private SubTaskDao subTaskDao;
    private TaskDao taskDao;
    private TaskManager taskManager;
    private Task.TaskBuilder taskBuilder;
    private SubTaskManager subTaskManager;

    @BeforeAll
    static void initTestDataSource() {
        dataSource = new EmbeddedDataSource();
        dataSource.setDatabaseName("memory:todo-test");
        dataSource.setCreateDatabase("create");
    }

    @BeforeEach
    void setupDao() {
        taskDao = new TaskDao(dataSource, subTaskDao, Mockito.mock(DependencyDao.class),
                Mockito.mock(TaskCategoryDao.class), Mockito.mock(CategoryDao.class), Mockito.mock(CategoryTimeDao.class));
        taskManager = new TaskManager(dataSource);
        taskBuilder = Task.builder("Testing task", 10L, LocalDate.EPOCH);

        subTaskDao = new SubTaskDao(dataSource);
        subTaskManager = new SubTaskManager(dataSource);
    }

    @AfterEach
    void cleanUp() {
        subTaskManager.dropTable();
        taskManager.dropTable();
    }

    @Test
    void test_getAllEntitiesFor_ExistingTaskWithSubtasks_SubTasksFromTaskReturned() {
        var task = taskBuilder.build();
        taskDao.add(task);
        var subTasks = setupSubtasks(task.getId());
        Assertions.assertThat(subTaskDao.getAllSubTasksForTask(task.getId())).isEqualTo(subTasks);
    }

    @Test
    void test_deleteAllEntitiesFor_ExistingTaskWIthSubTasks_AllSubtasksDeleted() {
        var task = taskBuilder.build();
        taskDao.add(task);
        setupSubtasks(task.getId());
        subTaskDao.deleteAllSubTasksForTask(task.getId());
        Assertions.assertThat(subTaskDao.getAllSubTasksForTask(task.getId())).isEmpty();
    }

    @Test
    void test_addEntityFor_ExistingTaskWithoutSubTasks_SubTasksAdded() {
        var task = taskBuilder.build();
        taskDao.add(task);
        var subTasks = new LinkedList<>(Arrays.asList(
                new SubTask("A", false),
                new SubTask("B", true),
                new SubTask("C", false)));
        for (var subTask : subTasks) {
            subTaskDao.addSubTaskForTask(task.getId(), subTask);
        }
        Assertions.assertThat(subTaskDao.getAllSubTasksForTask(task.getId())).isEqualTo(subTasks);
    }

    @Test
    void test_update_ExistingSubTask_SubTaskUpdated() {
        var task = taskBuilder.build();
        taskDao.add(task);
        var subTasks = setupSubtasks(task.getId());
        subTasks.get(0).setTitle("New TITLE");
        subTasks.get(0).toggleIsCompleted();
        subTaskDao.update(subTasks.get(0));
        Assertions.assertThat(subTaskDao.getAllSubTasksForTask(task.getId())).isEqualTo(subTasks);
    }

    @Test
    void test_delete_ExistingSubtask_SubTaskDeleted() {
        var task = taskBuilder.build();
        taskDao.add(task);
        var subTasks = setupSubtasks(task.getId());
        subTaskDao.delete(subTasks.get(0));
        subTasks.remove(0);
        Assertions.assertThat(subTaskDao.getAllSubTasksForTask(task.getId())).isEqualTo(subTasks);
    }

    @Test
    void test_fetch_ExistingSubTask_SubTaskFetched() {
        var task = taskBuilder.build();
        taskDao.add(task);
        var subTasks = setupSubtasks(task.getId());
        var fetchedSubtask = subTaskDao.findById(subTasks.get(0).getId());
        Assertions.assertThat(fetchedSubtask).isEqualTo(subTasks.get(0));
    }

    private List<SubTask> setupSubtasks(long taskId) {
        var subTasks = new LinkedList<>(Arrays.asList(
                new SubTask("Testing subtask", false),
                new SubTask("Testing subtask", false),
                new SubTask("Testing subtask", false),
                new SubTask("Testing subtask", false),
                new SubTask("Testing subtask", false))
        );
        for (var subTask : subTasks) {
            subTaskDao.addSubTaskForTask(taskId, subTask);

        }
        return subTasks;
    }
}
