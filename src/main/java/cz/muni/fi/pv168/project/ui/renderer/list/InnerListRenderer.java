package cz.muni.fi.pv168.project.ui.renderer.list;

import javax.swing.*;
import javax.swing.border.EtchedBorder;
import java.awt.*;
import java.util.function.Function;

public class InnerListRenderer<T> implements ListCellRenderer<T> {
    private final Function<T, String> stringGetter;
    private final int fontStyle;

    public InnerListRenderer(Function<T, String> stringGetter, int fontStyle) {
        this.stringGetter = stringGetter;
        this.fontStyle = fontStyle;
    }

    @Override
    public Component getListCellRendererComponent(JList<? extends T> list, T t, int index, boolean isSelected, boolean cellHasFocus) {
        JPanel panel = new JPanel(new FlowLayout(FlowLayout.LEFT));
        panel.setBorder(new EtchedBorder());

        JLabel label = new JLabel(stringGetter.apply(t));
        label.setFont(label.getFont().deriveFont(fontStyle));
        if (fontStyle != Font.PLAIN) { // prevent styled font cut-off by padding with a space
            label.setText(label.getText() + " ");
        }

        panel.add(label);

        if (isSelected) {
            panel.setBackground(list.getSelectionBackground());
            panel.setForeground(list.getSelectionForeground());
        } else {
            panel.setBackground(list.getBackground());
            panel.setForeground(list.getForeground());
        }

        return panel;
    }
}
