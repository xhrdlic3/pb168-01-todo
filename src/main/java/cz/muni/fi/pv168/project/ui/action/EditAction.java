package cz.muni.fi.pv168.project.ui.action;

import cz.muni.fi.pv168.project.data.task.Task;
import cz.muni.fi.pv168.project.ui.i18n.I18N;
import cz.muni.fi.pv168.project.ui.main.tab.ListHolder;
import org.jetbrains.annotations.Nullable;

import javax.swing.Icon;
import javax.swing.KeyStroke;
import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;
import java.util.List;
import java.util.function.Supplier;

public final class EditAction<T> extends AbstractAction<T> {

    private static final cz.muni.fi.pv168.project.ui.i18n.I18N I18N = new I18N(EditAction.class);

    public EditAction(ListHolder<T> listHolder, Icon icon, String description, @Nullable Supplier<Task> taskSupplier) {
        super(listHolder, selectedRowsCount -> selectedRowsCount == 1, taskSupplier);

        putValue(NAME, I18N.getString("editAction"));
        putValue(SMALL_ICON, icon);
        putValue(SHORT_DESCRIPTION, description);
        putValue(MNEMONIC_KEY, KeyEvent.VK_E);
        putValue(ACCELERATOR_KEY, KeyStroke.getKeyStroke("ctrl E"));
    }

    public EditAction(ListHolder<T> listHolder, Icon icon, String description) {
        this(listHolder, icon, description, () -> null);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        var list = getList();

        List<?> selectedRows = list.getSelectedValuesList();
        if (selectedRows.size() != 1) {
            throw new IllegalStateException("Invalid selected rows count (must be 1): " + selectedRows.size());
        }

        var optionalResult = getDialogFactory()
                .newEditDialog(list.getSelectedValue())
                .show(I18N.getString("editAction"));

        optionalResult.ifPresent(t -> {
            listHolder.getModel().setElementAt(t, listHolder.getModel().indexOf(t));
            triggerUpdate();
        });
    }
}
