package cz.muni.fi.pv168.project.ui.action;

import cz.muni.fi.pv168.project.ui.i18n.I18N;
import cz.muni.fi.pv168.project.ui.main.tab.ListHolder;

import javax.swing.Icon;
import javax.swing.KeyStroke;
import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;

public final class AddAction<T> extends AbstractAction<T> {

    private static final I18N I18N = new I18N(AddAction.class);

    public AddAction(ListHolder<T> listHolder, Icon icon, String description) {
        super(listHolder, selectedRowsCount -> true);

        putValue(NAME, I18N.getString("addAction"));
        putValue(SMALL_ICON, icon);
        putValue(SHORT_DESCRIPTION, description);
        putValue(MNEMONIC_KEY, KeyEvent.VK_A);
        putValue(ACCELERATOR_KEY, KeyStroke.getKeyStroke("ctrl N"));
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        var optionalResult = getDialogFactory()
                .newAddDialog()
                .show(I18N.getString("addAction"));

        optionalResult.ifPresent(t -> {
            listHolder.getModel().addElement(t);
            triggerUpdate();
        });
    }
}
