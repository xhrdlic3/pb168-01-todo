package cz.muni.fi.pv168.project.ui.action.task;

import cz.muni.fi.pv168.project.data.task.Completable;
import cz.muni.fi.pv168.project.data.task.Task;
import cz.muni.fi.pv168.project.ui.action.AbstractAction;
import cz.muni.fi.pv168.project.ui.i18n.I18N;
import cz.muni.fi.pv168.project.ui.main.tab.ListHolder;
import org.jetbrains.annotations.Nullable;

import javax.swing.KeyStroke;
import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;
import java.util.List;
import java.util.function.Supplier;

public class ToggleCompletionAction<T extends Completable> extends AbstractAction<T> {

    private static final I18N I18N = new I18N(ToggleCompletionAction.class);

    public ToggleCompletionAction(ListHolder<T> listHolder, String description, @Nullable Supplier<Task> taskSupplier) {
        super(listHolder, selectedRowsCount -> selectedRowsCount == 1, taskSupplier);

        putValue(NAME, I18N.getString("toggleAction"));
        putValue(SHORT_DESCRIPTION, description);
        putValue(MNEMONIC_KEY, KeyEvent.VK_T);
        putValue(ACCELERATOR_KEY, KeyStroke.getKeyStroke("ctrl T"));
    }

    public ToggleCompletionAction(ListHolder<T> listHolder, String description) {
        this(listHolder, description, null);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        var list = getList();

        List<?> selectedRows = list.getSelectedValuesList();
        if (selectedRows.size() != 1) {
            throw new IllegalStateException("Invalid selected rows count (must be 1): " + selectedRows.size());
        }

        var value = list.getSelectedValue();
        value.toggleIsCompleted();
        listHolder.getModel().setElementAt(value, listHolder.getModel().indexOf(value));
        triggerUpdate();
    }
}
