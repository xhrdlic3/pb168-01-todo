package cz.muni.fi.pv168.project.ui.main.toolbar;

import cz.muni.fi.pv168.project.model.LocalDateModel;
import cz.muni.fi.pv168.project.ui.action.TabActions;
import cz.muni.fi.pv168.project.ui.i18n.I18N;
import org.jdatepicker.JDatePicker;

import javax.swing.Action;
import javax.swing.Box;
import javax.swing.JLabel;
import javax.swing.JToolBar;
import java.awt.Font;
import java.awt.event.ActionListener;
import java.time.LocalDate;

public class CategoriesStatisticsToolBar {

    private final JToolBar toolbar;

    private final JDatePicker showStatisticsFrom;
    private final JDatePicker showStatisticsTo;

    private static final I18N I18N = new I18N(CategoriesStatisticsToolBar.class);

    public CategoriesStatisticsToolBar(TabActions tabActions) {
        toolbar = new JToolBar(JToolBar.HORIZONTAL);
        toolbar.setFloatable(false);

        showStatisticsFrom = new JDatePicker(new LocalDateModel());
        showStatisticsTo = new JDatePicker(new LocalDateModel());

        JLabel fromText = new JLabel(I18N.getString("from"));
        fromText.setFont(fromText.getFont().deriveFont(Font.BOLD));

        JLabel toText = new JLabel(I18N.getString("to"));
        toText.setFont(toText.getFont().deriveFont(Font.BOLD));

        for (Action action : tabActions.getLeftSideActions()) {
            toolbar.add(action);
        }

        toolbar.add(Box.createHorizontalGlue());
        toolbar.add(fromText);
        toolbar.add(Box.createHorizontalStrut(5));
        toolbar.add(showStatisticsFrom);
        toolbar.add(Box.createHorizontalStrut(5));
        toolbar.add(toText);
        toolbar.add(Box.createHorizontalStrut(5));
        toolbar.add(showStatisticsTo);
    }

    public JToolBar getToolBar() {
        return toolbar;
    }

    public void addDateActionListener(ActionListener l) {
        showStatisticsFrom.addActionListener(l);
        showStatisticsTo.addActionListener(l);
    }

    public LocalDate getFromDate() {
        return (LocalDate) showStatisticsFrom.getModel().getValue();
    }

    public LocalDate getToDate() {
        return (LocalDate) showStatisticsTo.getModel().getValue();
    }
}
