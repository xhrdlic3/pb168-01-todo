package cz.muni.fi.pv168.project.ui.main.view;

import cz.muni.fi.pv168.project.data.category.Category;
import cz.muni.fi.pv168.project.model.db.UpdatableDataModel;
import cz.muni.fi.pv168.project.ui.renderer.list.InnerColoredListRenderer;

import javax.swing.JComponent;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.border.EtchedBorder;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;

public class CategoryView {

    private final JList<Category> categoryList;
    private final JPanel panel;

    public CategoryView(UpdatableDataModel<Category> categoryModel) {
        panel = new JPanel(new GridBagLayout());

        categoryList = new JList<>(categoryModel);
        categoryList.setCellRenderer(new InnerColoredListRenderer<>(Category::getName, Category::getColor, Font.BOLD));
        categoryList.setBorder(new EtchedBorder());

        GridBagConstraints gbc = new GridBagConstraints();
        gbc.fill = GridBagConstraints.BOTH;
        gbc.anchor = GridBagConstraints.NORTHWEST;
        gbc.insets = new Insets(5, 5, 5, 5);
        gbc.weightx = 1;
        gbc.weighty = 1;
        gbc.gridy = 0;

        panel.add(categoryList, gbc);
    }

    public JList<Category> getCategoryList() {
        return categoryList;
    }

    public JComponent getComponent() {
        return panel;
    }
}
