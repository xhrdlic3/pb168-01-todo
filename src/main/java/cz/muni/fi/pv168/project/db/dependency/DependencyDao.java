package cz.muni.fi.pv168.project.db.dependency;

import cz.muni.fi.pv168.project.db.DataAccessException;

import javax.sql.DataSource;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class DependencyDao {

    private final DataSource dataSource;

    public DependencyDao(DataSource dataSource) {
        this.dataSource = dataSource;
    }

    public List<Long> getAllDependencyIdsForTaskId(Long sourceTaskId) {
        try (var connection = dataSource.getConnection();
             var st = connection.prepareStatement("SELECT ID_TARGET FROM TASK_DEPENDENCY WHERE ID_SOURCE = ?")) {
            st.setLong(1, sourceTaskId);

            List<Long> dependencyIds = new ArrayList<>();
            try (var rs = st.executeQuery()) {
                while (rs.next()) {
                    dependencyIds.add(rs.getLong("ID_TARGET"));
                }

                return dependencyIds;
            }
        } catch (SQLException ex) {
            throw new DataAccessException("Failed to load dependencies with task id " + sourceTaskId, ex);
        }
    }

    public void addDependencyAssociationFor(Long sourceTaskId, Long targetTaskId) {
        if (sourceTaskId == null) {
            throw new IllegalArgumentException("Source task has null ID");
        }
        if (targetTaskId == null) {
            throw new IllegalArgumentException("Dependency task has null ID");
        }

        try (var connection = dataSource.getConnection();
             var st = connection.prepareStatement(
                     "INSERT INTO TASK_DEPENDENCY (ID_SOURCE, ID_TARGET) VALUES (?, ?)")) {
            st.setLong(1, sourceTaskId);
            st.setLong(2, targetTaskId);

            st.executeUpdate();
        } catch (SQLException ex) {
            throw new DataAccessException("Failed to store dependency between IDs: " + sourceTaskId + " -> " + targetTaskId, ex);
        }
    }

    public void deleteDependencyAssociationFor(Long sourceTaskId, Long targetTaskId) {
        if (sourceTaskId == null) {
            throw new IllegalArgumentException("Source task has null ID");
        }
        if (targetTaskId == null) {
            throw new IllegalArgumentException("Dependency task has null ID");
        }

        try (var connection = dataSource.getConnection();
             var st = connection.prepareStatement("DELETE FROM TASK_DEPENDENCY WHERE ID_SOURCE = ? AND ID_TARGET = ?")) {
            st.setLong(1, sourceTaskId);
            st.setLong(2, targetTaskId);

            int rowsDeleted = st.executeUpdate();
            if (rowsDeleted == 0) {
                throw new DataAccessException(
                        "Failed to delete non-existing task dependency between IDs: " + sourceTaskId + " -> " + targetTaskId
                );
            }
        } catch (SQLException ex) {
            throw new DataAccessException("Failed to delete task dependency between IDs: " + sourceTaskId + " -> " + targetTaskId, ex);
        }
    }

    public void deleteAllDependencyAssociationsForTask(Long sourceTaskId) {
        var ids = getAllDependencyIdsForTaskId(sourceTaskId);
        for (var id : ids) {
            deleteDependencyAssociationFor(sourceTaskId, id);
        }
    }
}
