package cz.muni.fi.pv168.project.model;

import org.jdatepicker.AbstractDateModel;
import org.jdatepicker.DateModel;

import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Calendar;
import java.util.GregorianCalendar;

public class LocalDateModel extends AbstractDateModel<LocalDate> implements DateModel<LocalDate> {

    public LocalDateModel() {
        setValue(LocalDate.now());
    }

    @Override
    protected Calendar toCalendar(LocalDate localDate) {
        return GregorianCalendar.from(localDate.atStartOfDay(ZoneId.systemDefault()));
    }

    @Override
    protected LocalDate fromCalendar(Calendar calendar) {
        return calendar.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
    }
}
