package cz.muni.fi.pv168.project.data.task;

import cz.muni.fi.pv168.project.data.IdentifiableEntity;

import java.util.Objects;

public class SubTask implements Completable, IdentifiableEntity {

    private Long id;
    private String title;
    private boolean isCompleted;

    public SubTask(String title) {
        this(title, false);
    }

    public SubTask(String title, boolean isCompleted) {
        this.title = title;
        this.isCompleted = isCompleted;
    }

    @Override
    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    @Override
    public boolean isCompleted() {
        return isCompleted;
    }

    @Override
    public void toggleIsCompleted() {
        isCompleted = !isCompleted;
    }

    @Override
    public String toString() {
        return this.title;
    }

    @Override
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        SubTask subTask = (SubTask) o;
        return this.id != null && this.id.equals(subTask.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }
}
