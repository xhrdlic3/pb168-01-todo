package cz.muni.fi.pv168.project.db.taskcategory;

import cz.muni.fi.pv168.project.db.interfaces.AbstractDataAccessManager;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.SQLException;

public class TaskCategoryManager extends AbstractDataAccessManager {

    private final DataSource dataSource;

    public TaskCategoryManager(DataSource dataSource) {
        this.dataSource = dataSource;

        if (tableDoesNotExist()) {
            createTable();
        }
    }

    @Override
    public Connection getConnection() throws SQLException {
        return dataSource.getConnection();
    }

    @Override
    public String getTableName() {
        return "TASK_CATEGORY";
    }

    @Override
    public String getCreateTableStatement() {
        return "CREATE TABLE " + getTableName() + "(" +
                "ID BIGINT PRIMARY KEY GENERATED ALWAYS AS IDENTITY," +
                "ID_TASK BIGINT REFERENCES TASK(ID)," +
                "ID_CATEGORY BIGINT REFERENCES CATEGORY(ID)" +
                ")";
    }
}
