package cz.muni.fi.pv168.project.ui.action;

import javax.swing.Action;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public final class TabActions {

    private final List<Action> leftSideActions;

    private TabActions(List<Action> leftSideActions) {
        this.leftSideActions = Objects.requireNonNull(leftSideActions);
    }

    public static EntityActionsBuilder builder() {
        return new EntityActionsBuilder();
    }

    public List<Action> getLeftSideActions() {
        return leftSideActions;
    }

    public static final class EntityActionsBuilder {

        private final List<Action> leftSideActions;

        private EntityActionsBuilder() {
            leftSideActions = new ArrayList<>();
        }

        public EntityActionsBuilder addLeftSideAction(Action action) {
            this.leftSideActions.add(action);
            return this;
        }

        public TabActions build() {
            return new TabActions(leftSideActions);
        }
    }
}
