package cz.muni.fi.pv168.project.db.task;

import cz.muni.fi.pv168.project.db.interfaces.AbstractDataAccessManager;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.SQLException;

public class TaskManager extends AbstractDataAccessManager {

    private final DataSource dataSource;

    public TaskManager(DataSource dataSource) {
        this.dataSource = dataSource;

        if (tableDoesNotExist()) {
            createTable();
        }
    }

    @Override
    public String getTableName() {
        return "TASK";
    }

    @Override
    public String getCreateTableStatement() {
        return "CREATE TABLE " + getTableName() + "(" +
                "ID BIGINT PRIMARY KEY GENERATED ALWAYS AS IDENTITY," +
                "TITLE VARCHAR(256)," +
                "DUE_DATE DATE," +
                "ESTIMATED_TIME BIGINT," +
                "DESCRIPTION VARCHAR(1024)," +
                "TASK_STATUS VARCHAR(32)" +
                ")";
    }

    @Override
    public Connection getConnection() throws SQLException {
        return dataSource.getConnection();
    }
}
