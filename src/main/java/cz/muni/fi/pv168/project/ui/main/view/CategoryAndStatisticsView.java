package cz.muni.fi.pv168.project.ui.main.view;

import cz.muni.fi.pv168.project.data.category.Category;
import cz.muni.fi.pv168.project.db.DaoHolder;
import cz.muni.fi.pv168.project.model.db.UpdatableDataModel;
import cz.muni.fi.pv168.project.ui.action.AddAction;
import cz.muni.fi.pv168.project.ui.action.DeleteAction;
import cz.muni.fi.pv168.project.ui.action.EditAction;
import cz.muni.fi.pv168.project.ui.action.TabActions;
import cz.muni.fi.pv168.project.ui.dialog.factory.CategoryDialogFactory;
import cz.muni.fi.pv168.project.ui.dialog.factory.DialogFactory;
import cz.muni.fi.pv168.project.ui.i18n.I18N;
import cz.muni.fi.pv168.project.ui.main.tab.Tab;
import cz.muni.fi.pv168.project.ui.main.toolbar.CategoriesStatisticsToolBar;
import cz.muni.fi.pv168.project.ui.popup.PopupMenuFactory;
import cz.muni.fi.pv168.project.ui.resources.Icons;

import javax.swing.JComponent;
import javax.swing.JList;
import javax.swing.JSplitPane;
import javax.swing.JToolBar;

public class CategoryAndStatisticsView implements Tab<Category> {

    private final UpdatableDataModel<Category> categoryModel;

    private final CategoriesStatisticsToolBar toolBar;

    private final JSplitPane splitPane;

    private final CategoryView categoryView;
    private final StatisticsView statisticsView;

    private static final I18N I18N = new I18N(CategoryAndStatisticsView.class);

    public CategoryAndStatisticsView(DaoHolder daoHolder) {
        categoryModel = new UpdatableDataModel<>(daoHolder.getCategoryDao());
        splitPane = new JSplitPane(JSplitPane.HORIZONTAL_SPLIT);

        this.categoryView = new CategoryView(categoryModel);
        this.statisticsView = new StatisticsView(categoryModel);

        TabActions tabActions = TabActions.builder()
                .addLeftSideAction(new AddAction<>(this, Icons.ADD_CATEGORY_ICON, I18N.getString("addActionDesc")))
                .addLeftSideAction(new EditAction<>(this, Icons.EDIT_CATEGORY_ICON, I18N.getString("editActionDesc")))
                //TODO: needs refactor with a variable holding selectedCount of categories to be deleted for a proper localisation
                .addLeftSideAction(new DeleteAction<>(this, Icons.REMOVE_CATEGORY_BIN_ICON, I18N.getString("deleteActionDesc"))
                        .addEnabledCondition(selectedCount -> daoHolder
                                .getTaskDao()
                                .getAll()
                                .stream()
                                .noneMatch(task -> task.getCategories().contains(getList().getSelectedValue())))
                ).build();
        toolBar = new CategoriesStatisticsToolBar(tabActions);
        setupToolBar();

        PopupMenuFactory.builder()
                .addMenuItems(tabActions.getLeftSideActions())
                .buildFor(categoryView.getCategoryList());

        splitPane.setLeftComponent(categoryView.getComponent());
        splitPane.setRightComponent(statisticsView.getComponent());
        splitPane.setDividerLocation(300);
    }

    private void setupToolBar() {
        toolBar.addDateActionListener(actionEvent -> statisticsView.updateTable(toolBar.getFromDate(), toolBar.getToDate()));
    }

    @Override
    public String getLabel() {
        return I18N.getString("label");
    }

    @Override
    public JComponent getComponent() {
        return splitPane;
    }

    @Override
    public JToolBar getToolbar() {
        return toolBar.getToolBar();
    }

    @Override
    public JList<Category> getList() {
        return categoryView.getCategoryList();
    }

    @Override
    public DialogFactory<Category> getDialogFactory() {
        return new CategoryDialogFactory();
    }

    @Override
    public UpdatableDataModel<Category> getModel() {
        return categoryModel;
    }
}
