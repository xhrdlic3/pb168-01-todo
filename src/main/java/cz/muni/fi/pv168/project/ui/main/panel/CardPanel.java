package cz.muni.fi.pv168.project.ui.main.panel;

import javax.swing.JComponent;
import javax.swing.JPanel;
import java.awt.CardLayout;

public class CardPanel {
    private final CardLayout cardLayout;
    private final JPanel outerPanel;

    public CardPanel() {
        cardLayout = new CardLayout();
        outerPanel = new JPanel(cardLayout);
    }

    public CardPanel addComponent(JComponent component, String label) {
        outerPanel.add(component, label);
        return this;
    }

    public void showComponent(String label) {
        cardLayout.show(outerPanel, label);
    }

    public JPanel getContainer() {
        return outerPanel;
    }
}
