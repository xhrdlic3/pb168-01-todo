package cz.muni.fi.pv168.project.model.db;

import cz.muni.fi.pv168.project.data.task.Task;

import java.util.List;
import java.util.function.Function;

public abstract class AbstractTaskPropertyListDataModel<E> extends AbstractDataModel<E> {

    protected Task parentTask;
    private final Function<Task, List<E>> fetcher;

    public AbstractTaskPropertyListDataModel(Function<Task, List<E>> fetcher) {
        this.fetcher = fetcher;
    }

    public void setParentTask(Task task) {
        this.parentTask = task;
    }

    public Task getParentTask() {
        return parentTask;
    }

    public List<E> fetchAll() {
        return fetcher.apply(parentTask);
    }
}
