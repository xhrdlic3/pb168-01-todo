package cz.muni.fi.pv168.project.data.task;

import cz.muni.fi.pv168.project.ui.i18n.I18N;

public enum TaskStatus {
    PLANNED,
    IN_PROGRESS,
    FINISHED;

    private static final I18N I18N = new I18N(TaskStatus.class);

    @Override
    public String toString() {
        return I18N.getString(this);
    }

    public String upperCaseString() {
        return this.name();
    }
}
