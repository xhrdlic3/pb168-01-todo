package cz.muni.fi.pv168.project.ui.dialog.factory;

import cz.muni.fi.pv168.project.data.category.Category;
import cz.muni.fi.pv168.project.db.interfaces.DataAccessObject;
import cz.muni.fi.pv168.project.ui.dialog.AbstractLaidOutDialog;
import cz.muni.fi.pv168.project.ui.dialog.CategoryDialog;
import cz.muni.fi.pv168.project.ui.dialog.ComboBoxDialog;

public class ExistingCategoryDialogFactory implements DialogFactory<Category> {

    private final DataAccessObject<Category> categoryDao;

    public ExistingCategoryDialogFactory(DataAccessObject<Category> categoryDao) {
        this.categoryDao = categoryDao;
    }

    @Override
    public AbstractLaidOutDialog<Category> newEditDialog(Category entity) {
        return new CategoryDialog(entity);
    }

    @Override
    public AbstractLaidOutDialog<Category> newAddDialog() {
        return new ComboBoxDialog<>(categoryDao.getAll());
    }
}
