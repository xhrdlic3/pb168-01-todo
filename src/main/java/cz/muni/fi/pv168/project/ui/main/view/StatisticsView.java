package cz.muni.fi.pv168.project.ui.main.view;

import cz.muni.fi.pv168.project.data.category.Category;
import cz.muni.fi.pv168.project.model.CategoryStatisticsTableModel;
import cz.muni.fi.pv168.project.model.db.UpdatableDataModel;
import cz.muni.fi.pv168.project.ui.renderer.statistics.CategoryStatisticsRenderer;
import cz.muni.fi.pv168.project.ui.renderer.statistics.TimeSpentStatisticsRenderer;

import javax.swing.JComponent;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import java.time.LocalDate;

public class StatisticsView {

    private final JScrollPane scrollPane;
    private final CategoryStatisticsTableModel tableModel;

    public StatisticsView(UpdatableDataModel<Category> categoryModel) {
        tableModel = new CategoryStatisticsTableModel(categoryModel);
        JTable statisticsTable = new JTable(tableModel);

        statisticsTable.getColumnModel().getColumn(0).setCellRenderer(new CategoryStatisticsRenderer());
        statisticsTable.getColumnModel().getColumn(1).setCellRenderer(new TimeSpentStatisticsRenderer());
        statisticsTable.setRowHeight(40);
        statisticsTable.getTableHeader().setReorderingAllowed(false);

        scrollPane = new JScrollPane();
        scrollPane.setViewportView(statisticsTable);
    }

    public JComponent getComponent() {
        return scrollPane;
    }

    public void updateTable(LocalDate fromDate, LocalDate toDate) {
        tableModel.setFilter(fromDate, toDate);
        tableModel.fireTableDataChanged();
    }
}
