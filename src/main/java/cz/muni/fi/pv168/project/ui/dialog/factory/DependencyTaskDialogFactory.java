package cz.muni.fi.pv168.project.ui.dialog.factory;

import cz.muni.fi.pv168.project.data.IdentifiableEntity;
import cz.muni.fi.pv168.project.data.task.Task;
import cz.muni.fi.pv168.project.db.dependency.DependencyDao;
import cz.muni.fi.pv168.project.db.interfaces.DataAccessObject;
import cz.muni.fi.pv168.project.ui.dialog.AbstractLaidOutDialog;
import cz.muni.fi.pv168.project.ui.dialog.ComboBoxDialog;

import java.util.stream.Collectors;

public class DependencyTaskDialogFactory implements DialogFactory<Task> {

    private final DataAccessObject<Task> taskDao;
    private final DependencyDao dependencyDao;

    private IdentifiableEntity sourceEntity;

    public DependencyTaskDialogFactory(DependencyDao dependencyDao, DataAccessObject<Task> taskDao) {
        this.taskDao = taskDao;
        this.dependencyDao = dependencyDao;
    }

    @Override
    public AbstractLaidOutDialog<Task> newEditDialog(Task entity) {
        throw new UnsupportedOperationException();
    }

    @Override
    public AbstractLaidOutDialog<Task> newAddDialog() {
        var allowedTasks = taskDao
                .getAll()
                .stream()
                .filter(this::isLoopFree)
                .filter(this::isNewDependency)
                .collect(Collectors.toList());
        return new ComboBoxDialog<>(allowedTasks);
    }

    private boolean isNewDependency(Task otherTask) {
        for (Long targetId : dependencyDao.getAllDependencyIdsForTaskId(sourceEntity.getId())) {
            if (otherTask.getId().equals(targetId)) {
                return false;
            }
        }

        return true;
    }

    private boolean isLoopFree(Task otherTask) {
        if (otherTask.getId().equals(sourceEntity.getId())) {
            return false;
        }

        for (Long targetId : dependencyDao.getAllDependencyIdsForTaskId(otherTask.getId())) {
            if (targetId.equals(sourceEntity.getId())) {
                return false;
            }
        }

        return true;
    }

    public void setSourceEntity(IdentifiableEntity sourceEntity) {
        this.sourceEntity = sourceEntity;
    }
}
