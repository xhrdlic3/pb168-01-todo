package cz.muni.fi.pv168.project.data;

public interface IdentifiableEntity {

    Long getId();
}
