package cz.muni.fi.pv168.project.ui.dialog;

import cz.muni.fi.pv168.project.ui.centering.WindowCenterer;
import cz.muni.fi.pv168.project.ui.i18n.I18N;
import cz.muni.fi.pv168.project.ui.resources.Icons;

import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.util.Optional;

abstract class AbstractEntityDialog<E> extends JDialog {

    private final JPanel panel;
    private final JButton okButton;

    private static final I18N I18N = new I18N(AbstractEntityDialog.class);

    private boolean wasOk = false;

    AbstractEntityDialog() {
        panel = new JPanel();
        panel.setLayout(new GridBagLayout());

        okButton = new JButton(I18N.getString("buttonConfirm"));
        okButton.addActionListener(e -> {
            wasOk = true;
            this.dispose();
        });
        JButton cancelButton = new JButton(I18N.getString("buttonDismiss"));
        cancelButton.addActionListener(e -> {
            wasOk = false;
            this.dispose();
        });

        GridBagConstraints gbc = new GridBagConstraints();
        gbc.fill = GridBagConstraints.BOTH;
        gbc.anchor = GridBagConstraints.NORTHEAST;
        gbc.insets = new Insets(5, 5, 5, 5);
        gbc.weightx = 1;

        JPanel okCancelPanel = new JPanel(new GridBagLayout());
        okCancelPanel.add(Box.createHorizontalGlue(), gbc);

        gbc.weightx = 0;
        gbc.gridx = 1;
        okCancelPanel.add(okButton, gbc);

        gbc.gridx = 2;
        okCancelPanel.add(cancelButton, gbc);

        this.setTitle(I18N.getString("vmTitle"));
        this.setMinimumSize(new Dimension(300, 100));
        this.setDefaultCloseOperation(DISPOSE_ON_CLOSE);
        this.setModalityType(ModalityType.APPLICATION_MODAL);
        Icons.addTodoIconToWindow(this);

        this.setLayout(new BorderLayout(5, 5));
        this.getRootPane().setBorder(BorderFactory.createCompoundBorder(
                this.getRootPane().getBorder(),
                new EmptyBorder(10, 10, 10, 10)
        ));
        this.add(panel, BorderLayout.CENTER);
        this.add(okCancelPanel, BorderLayout.SOUTH);
    }

    abstract E getEntity();

    protected void addField(JComponent component, GridBagConstraints constraints) {
        panel.add(component, constraints);
    }

    protected void addField(JComponent component, GridBagConstraints constraints, int row) {
        constraints.gridy = row;
        panel.add(component, constraints);
    }

    protected void addLabeledField(String labelText, JComponent component, GridBagConstraints labelConstraints,
                                   GridBagConstraints componentConstraints, int row) {
        labelConstraints.gridy = row;
        componentConstraints.gridy = row;
        var label = new JLabel(labelText);
        label.setLabelFor(component);
        addField(label, labelConstraints);
        addField(component, componentConstraints);
    }

    public Optional<E> show(String title) {
        WindowCenterer.centerWindowToCurrentScreen(this);
        this.setTitle(title);

        this.pack();
        this.setVisible(true);

        return wasOk ? Optional.ofNullable(getEntity()) : Optional.empty();
    }

    protected JButton getOkButton() {
        return okButton;
    }
}
