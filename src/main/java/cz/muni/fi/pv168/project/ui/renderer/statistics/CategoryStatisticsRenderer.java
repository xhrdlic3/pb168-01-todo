package cz.muni.fi.pv168.project.ui.renderer.statistics;

import cz.muni.fi.pv168.project.data.category.Category;
import cz.muni.fi.pv168.project.ui.main.panel.CircleColorPanel;

import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JPanel;

public class CategoryStatisticsRenderer extends AbstractStatisticsRenderer<Category> {
    public CategoryStatisticsRenderer() {
        super(Category.class);
    }

    @Override
    protected JComponent modifyComponent(Category value) {
        final var panel = new JPanel();
        panel.add(new JLabel(value.getName()));
        panel.add(new CircleColorPanel(value.getColor()));
        return panel;
    }
}
