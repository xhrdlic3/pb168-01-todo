package cz.muni.fi.pv168.project.ui.popup;

import javax.swing.Action;
import javax.swing.JList;
import javax.swing.JPopupMenu;
import javax.swing.SwingUtilities;
import java.awt.Point;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.List;

public final class PopupMenuFactory {
    private PopupMenuFactory() {
    }

    public static PopupMenuBuilder builder() {
        return new PopupMenuBuilder();
    }

    public static final class PopupMenuBuilder {
        private final JPopupMenu menu = new JPopupMenu();

        private PopupMenuBuilder() {
        }

        public PopupMenuBuilder addMenuItem(Action action) {
            menu.add(action);
            return this;
        }

        public PopupMenuBuilder addMenuItems(List<Action> actions) {
            for (Action action : actions) {
                menu.add(action);
            }
            return this;
        }

        public PopupMenuBuilder addSeparator() {
            menu.addSeparator();
            return this;
        }

        public void buildFor(JList<?> list) {
            list.addMouseListener(new MouseAdapter() {
                @Override
                public void mouseReleased(MouseEvent e) {
                    if (SwingUtilities.isRightMouseButton(e)) {
                        Point p = e.getPoint();

                        list.setSelectedIndex(list.locationToIndex(p));
                        menu.show(list, p.x, p.y);
                    }
                }
            });
        }
    }
}
