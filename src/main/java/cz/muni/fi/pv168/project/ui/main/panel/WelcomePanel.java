package cz.muni.fi.pv168.project.ui.main.panel;

import cz.muni.fi.pv168.project.ui.i18n.I18N;

import javax.swing.BorderFactory;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.border.EmptyBorder;
import javax.swing.border.EtchedBorder;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;

public class WelcomePanel {

    private final JPanel panel;

    private static final I18N I18N = new I18N(WelcomePanel.class);
    private static final String WELCOME_MESSAGE = I18N.getString("welcomeMessage");

    public WelcomePanel() {
        var welcomeTextArea = new JTextArea(WELCOME_MESSAGE);
        welcomeTextArea.setFont(new Font(Font.SANS_SERIF, welcomeTextArea.getFont().getStyle(), 20));
        welcomeTextArea.setLineWrap(true);
        welcomeTextArea.setWrapStyleWord(true);
        welcomeTextArea.setEditable(false);
        welcomeTextArea.setMinimumSize(new Dimension(200, 50));

        panel = new JPanel(new BorderLayout());
        panel.setBorder(BorderFactory.createCompoundBorder(new EtchedBorder(EtchedBorder.LOWERED), new EmptyBorder(50, 50, 50, 50)));
        welcomeTextArea.setBackground(new Color(panel.getBackground().getRGB()));
        panel.add(welcomeTextArea);
    }

    public JPanel getPanel() {
        return panel;
    }
}
