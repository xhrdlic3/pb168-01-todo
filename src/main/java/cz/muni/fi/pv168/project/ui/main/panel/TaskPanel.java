package cz.muni.fi.pv168.project.ui.main.panel;

import cz.muni.fi.pv168.project.data.task.Task;
import cz.muni.fi.pv168.project.ui.action.UpdatablePanel;
import cz.muni.fi.pv168.project.ui.main.panel.listholder.CategoryHolderPanel;
import cz.muni.fi.pv168.project.ui.main.panel.listholder.DependencyHolderPanel;
import cz.muni.fi.pv168.project.ui.main.panel.listholder.SubTaskHolderPanel;
import org.jetbrains.annotations.Nullable;

import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.util.List;

public class TaskPanel extends JPanel implements UpdatablePanel {

    private final JLabel titleLabel;
    private final JTextArea descriptionArea;

    public TaskPanel(CategoryHolderPanel categoryHolder, SubTaskHolderPanel subTaskHolder, DependencyHolderPanel dependencyHolder) {
        super(new GridBagLayout());

        ensureSingleSelection(categoryHolder, subTaskHolder, dependencyHolder);

        titleLabel = new JLabel();
        titleLabel.setFont(titleLabel.getFont().deriveFont(Font.BOLD).deriveFont(titleLabel.getFont().getSize2D() * 1.5f));
        titleLabel.setMinimumSize(new Dimension(200, 20));

        descriptionArea = new JTextArea();
        descriptionArea.setLineWrap(true);
        descriptionArea.setWrapStyleWord(true);
        descriptionArea.setEditable(false);
        descriptionArea.setOpaque(false);
        descriptionArea.setFont(new Font("Arial", Font.ITALIC, 12));
        descriptionArea.setMinimumSize(new Dimension(200, 30));

        GridBagConstraints gbc = new GridBagConstraints();
        gbc.fill = GridBagConstraints.BOTH;
        gbc.anchor = GridBagConstraints.NORTHWEST;
        gbc.insets = new Insets(5, 5, 5, 5);
        gbc.weightx = 1;
        gbc.weighty = 0;

        gbc.gridy = 0;
        this.add(titleLabel, gbc);

        gbc.gridy = 1;
        this.add(descriptionArea, gbc);

        gbc.weighty = 1;
        gbc.gridy = 2;
        this.add(categoryHolder.getPanel(), gbc);

        gbc.gridy = 3;
        this.add(subTaskHolder.getPanel(), gbc);

        gbc.gridy = 4;
        this.add(dependencyHolder.getPanel(), gbc);
    }

    private void ensureSingleSelection(
            CategoryHolderPanel categoryHolder,
            SubTaskHolderPanel subTaskHolder,
            DependencyHolderPanel dependencyHolder
    ) {
        var allLists = List.of(categoryHolder.getList(), subTaskHolder.getList(), dependencyHolder.getList());
        for (JList<?> list : allLists) {
            list.addListSelectionListener(e -> {
                JList<?> source = (JList<?>) e.getSource();

                if (source.getSelectedIndex() != -1) {
                    for (JList<?> list1 : allLists) {
                        if (list1 != source) { // reference comparison actually what we need!
                            list1.clearSelection();
                        }
                    }
                }
            });
        }
    }

    public void updateFrom(@Nullable Task task) {
        if (task != null) {
            titleLabel.setText(task.getTitle());
            descriptionArea.setText(task.getDescription());
        }
    }
}
