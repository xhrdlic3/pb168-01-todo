package cz.muni.fi.pv168.project.ui.dialog.factory;

import cz.muni.fi.pv168.project.data.task.SubTask;
import cz.muni.fi.pv168.project.ui.dialog.AbstractLaidOutDialog;
import cz.muni.fi.pv168.project.ui.dialog.SubTaskDialog;

public class SubTaskDialogFactory implements DialogFactory<SubTask> {
    @Override
    public AbstractLaidOutDialog<SubTask> newEditDialog(SubTask entity) {
        return new SubTaskDialog(entity);
    }

    @Override
    public AbstractLaidOutDialog<SubTask> newAddDialog() {
        return new SubTaskDialog();
    }
}
