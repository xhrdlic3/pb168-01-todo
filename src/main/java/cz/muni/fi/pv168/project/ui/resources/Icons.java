package cz.muni.fi.pv168.project.ui.resources;

import cz.muni.fi.pv168.project.data.task.TaskStatus;

import javax.swing.Icon;
import javax.swing.ImageIcon;
import java.awt.Window;
import java.net.URL;
import java.util.logging.Level;
import java.util.logging.Logger;


public final class Icons {

    public static final Icon REMOVE_CATEGORY_BIN_ICON = createIcon("remove_category_32x32.png");
    public static final Icon REMOVE_TASK_BIN_ICON = createIcon("remove_task_32x32.png");

    public static final Icon EDIT_CATEGORY_ICON = createIcon("edit_category_32x32.png");
    public static final Icon EDIT_TASK_ICON = createIcon("edit_task_32x32.png");

    public static final Icon ADD_TASK_ICON = createIcon("add_task_32x32.png");
    public static final Icon ADD_CATEGORY_ICON = createIcon("add_category_32x32.png");

    public static final Icon TASK_STATUS_PLANNED_ICON = createIcon("task_status_planned_24x24.png");
    public static final Icon TASK_STATUS_IN_PROGRESS_ICON = createIcon("task_status_in_progress_24x24.png");
    public static final Icon TASK_STATUS_FINISHED_ICON = createIcon("task_status_finished_24x24.png");
    public static final Icon TASK_STATUS_URGENT_ICON = createIcon("task_status_urgent_24x24.png");

    public static final Icon TASK_STATUS_PLANNED_SMALL_ICON = createIcon("task_status_planned_16x16.png");
    public static final Icon TASK_STATUS_IN_PROGRESS_SMALL_ICON = createIcon("task_status_in_progress_16x16.png");
    public static final Icon TASK_STATUS_FINISHED_SMALL_ICON = createIcon("task_status_finished_16x16.png");
    public static final Icon TASK_STATUS_URGENT_SMALL_ICON = createIcon("task_status_urgent_16x16.png");

    public static final Icon LOGO_ICON = createIcon("logo_256x256.png");

    private Icons() {
        throw new AssertionError("This class is not instantiable.");
    }

    private static ImageIcon createIcon(String name) {
        URL url = Icons.class.getResource(name);
        if (url == null) {
            Logger.getLogger(Icon.class.getName()).log(Level.WARNING, "Icon resource '" + name + "'not found on classpath.");
            return null;
        }
        return new ImageIcon(url);
    }

    public static void addTodoIconToWindow(Window window) {
        if (Icons.LOGO_ICON != null) {
            window.setIconImage(((ImageIcon) Icons.LOGO_ICON).getImage());
        }
    }

    public static Icon getUrgencyIcon(boolean useBigIcons) {
        return useBigIcons ? Icons.TASK_STATUS_URGENT_ICON : Icons.TASK_STATUS_URGENT_SMALL_ICON;
    }

    public static Icon getTaskStatusIcon(TaskStatus status, boolean useBigIcons) {
        Icon result = null;

        switch (status) {
            case PLANNED:
                result = useBigIcons ? TASK_STATUS_PLANNED_ICON : TASK_STATUS_PLANNED_SMALL_ICON;
                break;
            case IN_PROGRESS:
                result = useBigIcons ? TASK_STATUS_IN_PROGRESS_ICON : TASK_STATUS_IN_PROGRESS_SMALL_ICON;
                break;
            case FINISHED:
                result = useBigIcons ? TASK_STATUS_FINISHED_ICON : TASK_STATUS_FINISHED_SMALL_ICON;
                break;
            default:
        }

        return result;
    }
}
