package cz.muni.fi.pv168.project.ui.action;

import cz.muni.fi.pv168.project.ui.i18n.I18N;
import cz.muni.fi.pv168.project.ui.main.tab.ListHolder;

import javax.swing.Icon;
import javax.swing.JOptionPane;
import javax.swing.KeyStroke;
import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;
import java.util.Collections;
import java.util.List;

public final class DeleteAction<T> extends AbstractAction<T> {

    private static final I18N I18N = new I18N(DeleteAction.class);

    public DeleteAction(ListHolder<T> listHolder, Icon icon, String description) {
        super(listHolder, selectedRowsCount -> selectedRowsCount >= 1);

        putValue(NAME, I18N.getString("deleteAction"));
        putValue(SMALL_ICON, icon);
        putValue(SHORT_DESCRIPTION, description);
        putValue(MNEMONIC_KEY, KeyEvent.VK_D);
        putValue(ACCELERATOR_KEY, KeyStroke.getKeyStroke("ctrl D"));
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        int result = JOptionPane.showConfirmDialog(listHolder.getList(), I18N.getString("confirmation"));
        if (result != JOptionPane.OK_OPTION) {
            return;
        }

        List<T> selectedRows = listHolder.getList().getSelectedValuesList();
        Collections.reverse(selectedRows);
        for (T selected : selectedRows) {
            listHolder.getModel().removeElement(selected);
        }
        triggerUpdate();
    }
}
