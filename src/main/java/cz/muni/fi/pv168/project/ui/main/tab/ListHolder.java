package cz.muni.fi.pv168.project.ui.main.tab;

import cz.muni.fi.pv168.project.ui.dialog.factory.DialogFactory;

import javax.swing.DefaultListModel;
import javax.swing.JList;

public interface ListHolder<T> {
    JList<T> getList();

    DialogFactory<T> getDialogFactory();

    DefaultListModel<T> getModel();
}
