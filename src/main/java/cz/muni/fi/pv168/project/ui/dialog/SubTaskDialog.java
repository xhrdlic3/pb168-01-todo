package cz.muni.fi.pv168.project.ui.dialog;

import cz.muni.fi.pv168.project.data.task.SubTask;
import cz.muni.fi.pv168.project.ui.i18n.I18N;

import javax.swing.JTextField;

public class SubTaskDialog extends AbstractLaidOutDialog<SubTask> {

    protected SubTask entity;
    protected final JTextField titleField = new JTextField();

    private static final I18N I18N = new I18N(SubTaskDialog.class);

    public SubTaskDialog() {
        entity = new SubTask("");
        this.addTitle();
    }

    public SubTaskDialog(SubTask initialValue) {
        this();

        entity = initialValue;

        titleField.setText(initialValue.getTitle());
    }

    protected void addTitle() {
        addLabeledField(I18N.getString("fieldLabelTitle"), titleField, labelGbc, componentGbc, getRowThenIncrement());
    }

    @Override
    SubTask getEntity() {
        entity.setTitle(titleField.getText());
        return entity;
    }
}
