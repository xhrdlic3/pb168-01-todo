package cz.muni.fi.pv168.project.ui.main.panel.listholder;

import cz.muni.fi.pv168.project.data.category.Category;
import cz.muni.fi.pv168.project.db.category.CategoryDao;
import cz.muni.fi.pv168.project.db.taskcategory.TaskCategoryDao;
import cz.muni.fi.pv168.project.model.db.TaskCategoryModel;
import cz.muni.fi.pv168.project.ui.action.AddAction;
import cz.muni.fi.pv168.project.ui.action.DeleteAction;
import cz.muni.fi.pv168.project.ui.action.EditAction;
import cz.muni.fi.pv168.project.ui.dialog.factory.DialogFactory;
import cz.muni.fi.pv168.project.ui.dialog.factory.ExistingCategoryDialogFactory;
import cz.muni.fi.pv168.project.ui.i18n.I18N;
import cz.muni.fi.pv168.project.ui.popup.PopupMenuFactory;
import cz.muni.fi.pv168.project.ui.renderer.list.InnerColoredListRenderer;
import cz.muni.fi.pv168.project.ui.resources.Icons;

import java.awt.Font;
import java.util.Optional;

public class CategoryHolderPanel extends AbstractHolderPanel<Category> {

    private final ExistingCategoryDialogFactory factory;

    private static final I18N I18N = new I18N(CategoryHolderPanel.class);

    public CategoryHolderPanel(TaskCategoryDao categoryDao, CategoryDao parentDao) {
        super(
                new TaskCategoryModel(categoryDao, parentDao), I18N.getString("panelName"),
                new InnerColoredListRenderer<>(Category::getName, Category::getColor, Font.BOLD | Font.ITALIC),
                Optional.empty());

        factory = new ExistingCategoryDialogFactory(parentDao);
        PopupMenuFactory.builder()
                .addMenuItem(new AddAction<>(this, Icons.ADD_CATEGORY_ICON,
                        I18N.getString("addCategoryOption")).addUpdatablePanel(this))
                .addMenuItem(new EditAction<>(this, Icons.EDIT_CATEGORY_ICON,
                        I18N.getString("editCategoryOption")).addUpdatablePanel(this))
                .addMenuItem(new DeleteAction<>(this, Icons.REMOVE_CATEGORY_BIN_ICON,
                        I18N.getString("removeCategoryOption")).addUpdatablePanel(this))
                .buildFor(this.getList());
    }

    @Override
    public DialogFactory<Category> getDialogFactory() {
        return factory;
    }
}
