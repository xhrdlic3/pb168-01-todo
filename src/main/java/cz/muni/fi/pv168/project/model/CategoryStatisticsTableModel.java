package cz.muni.fi.pv168.project.model;

import cz.muni.fi.pv168.project.data.category.Category;
import cz.muni.fi.pv168.project.ui.i18n.I18N;

import javax.swing.ListModel;
import javax.swing.event.ListDataEvent;
import javax.swing.event.ListDataListener;
import javax.swing.table.AbstractTableModel;
import java.time.LocalDate;

public class CategoryStatisticsTableModel extends AbstractTableModel {
    private final ListModel<Category> model;
    private LocalDate fromDate;
    private LocalDate toDate;

    private static final I18N I18N = new I18N(CategoryStatisticsTableModel.class);

    public CategoryStatisticsTableModel(ListModel<Category> model) {
        fromDate = LocalDate.now();
        toDate = LocalDate.now();
        this.model = model;

        model.addListDataListener(new ListModelListener(this));
    }

    @Override
    public int getRowCount() {
        return model.getSize();
    }

    @Override
    public int getColumnCount() {
        return 2;
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        var category = model.getElementAt(rowIndex);
        switch (columnIndex) {
            case 0:
                return category;
            case 1:
                return getTimeDurationFormatted(category);
            default:
                throw new IndexOutOfBoundsException("Invalid column index: " + columnIndex);
        }
    }

    public String getTimeDurationFormatted(Category category) {
        var duration = category.getTotalDuration(fromDate, toDate);
        return I18N.getFormattedMessage("timePeriodFormat", duration.toDays(), duration.toHours());
    }

    @Override
    public String getColumnName(int columnIndex) {
        switch (columnIndex) {
            case 0:
                return I18N.getString("columnNameTitle");
            case 1:
                return I18N.getString("columnTimeTitle");
            default:
                throw new IndexOutOfBoundsException("Invalid column index: " + columnIndex);
        }
    }

    public void setFilter(LocalDate fromFilterDate, LocalDate toFilterDate) {
        this.fromDate = fromFilterDate;
        this.toDate = toFilterDate;
    }

    private static final class ListModelListener implements ListDataListener {

        private final AbstractTableModel model;

        private ListModelListener(AbstractTableModel model) {
            this.model = model;
        }

        @Override
        public void intervalAdded(ListDataEvent listDataEvent) {
            model.fireTableRowsInserted(listDataEvent.getIndex0(), listDataEvent.getIndex1());
        }

        @Override
        public void intervalRemoved(ListDataEvent listDataEvent) {
            model.fireTableRowsDeleted(listDataEvent.getIndex0(), listDataEvent.getIndex1());
        }

        @Override
        public void contentsChanged(ListDataEvent listDataEvent) {
            model.fireTableDataChanged();
        }
    }
}
