package cz.muni.fi.pv168.project.db.categorytime;

import cz.muni.fi.pv168.project.data.category.CategoryTime;
import cz.muni.fi.pv168.project.db.DataAccessException;

import javax.sql.DataSource;
import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

public class CategoryTimeDao {

    private final DataSource dataSource;

    public CategoryTimeDao(DataSource dataSource) {
        this.dataSource = dataSource;
    }

    public List<CategoryTime> getAllCategoryTimesForTask(Long taskId) {
        return getAllCategoryTimesForCategory(taskId, "TASK_ID");
    }

    public List<CategoryTime> getAllCategoryTimesForCategory(Long categoryId) {
        return getAllCategoryTimesForCategory(categoryId, "CATEGORY_ID");
    }

    private List<CategoryTime> getAllCategoryTimesForCategory(Long id, String attribute) {
        if (id == null) {
            throw new IllegalArgumentException("Cannot look for categoryTime of null ID parent");
        }

        try (var connection = dataSource.getConnection();
             var st = connection.prepareStatement(
                     "SELECT ID, COMPLETION_DATE, HOURS_SPENT, TASK_ID FROM CATEGORYTIME WHERE " + attribute + " = ?"
             )) {
            st.setLong(1, id);

            List<CategoryTime> categoryTimes = new ArrayList<>();
            try (var rs = st.executeQuery()) {
                while (rs.next()) {
                    categoryTimes.add(toEntity(rs));
                }

                return categoryTimes;
            }
        } catch (SQLException ex) {
            throw new DataAccessException("Failed to load times with categoryTime id " + id, ex);
        }
    }

    public void deleteAllCategoryTimesForCategory(Long categoryId) {
        var categoryTimes = getAllCategoryTimesForCategory(categoryId);
        for (var categoryTime : categoryTimes) {
            delete(categoryTime);
        }
    }

    public void deleteAllCategoryTimesForTask(Long taskId) {
        var categoryTimes = getAllCategoryTimesForCategory(taskId, "TASK_ID");
        for (var categoryTime : categoryTimes) {
            delete(categoryTime);
        }
    }

    public void addCategoryTimeFor(Long categoryId, CategoryTime categoryTime) {
        if (categoryTime.getId() != null) {
            throw new IllegalArgumentException("CategoryTime already has ID: " + categoryTime);
        }

        try (var connection = dataSource.getConnection();
             var st = connection.prepareStatement(
                     "INSERT INTO CATEGORYTIME (CATEGORY_ID, TASK_ID, HOURS_SPENT, COMPLETION_DATE) VALUES (?, ?, ?, ?)",
                     Statement.RETURN_GENERATED_KEYS)) {
            st.setLong(1, categoryId);
            st.setLong(2, categoryTime.getTaskId());
            st.setLong(3, categoryTime.getTimeSpent().toHours());
            st.setDate(4, Date.valueOf(categoryTime.getCompletionDate()));

            st.executeUpdate();

            try (var rs = st.getGeneratedKeys()) {
                if (rs.getMetaData().getColumnCount() != 1) {
                    throw new DataAccessException("Failed to fetch generated key: compound key returned for categoryTime: " + categoryTime);
                }
                if (rs.next()) {
                    categoryTime.setId(rs.getLong(1));
                } else {
                    throw new DataAccessException("Failed to fetch generated key: no key returned for categoryTime: " + categoryTime);
                }
                if (rs.next()) {
                    throw new DataAccessException(
                            "Failed to fetch generated key: multiple keys returned for categoryTime: " + categoryTime
                    );
                }
            }
        } catch (SQLException ex) {
            throw new DataAccessException("Failed to store categoryTime " + categoryTime, ex);
        }
    }

    public void update(CategoryTime categoryTime) {
        if (categoryTime.getId() == null) {
            throw new IllegalArgumentException("Subtask has null ID: " + categoryTime);
        }

        try (var connection = dataSource.getConnection();
             var st = connection.prepareStatement(
                     "UPDATE CATEGORYTIME SET HOURS_SPENT = ?, COMPLETION_DATE = ?, TASK_ID = ? WHERE ID = ?")) {
            st.setLong(1, categoryTime.getTimeSpent().toHours());
            st.setDate(2, Date.valueOf(categoryTime.getCompletionDate()));
            st.setLong(3, categoryTime.getTaskId());
            st.setLong(4, categoryTime.getId());

            int rowsUpdated = st.executeUpdate();
            if (rowsUpdated == 0) {
                throw new DataAccessException("Failed to update non-existing subtask: " + categoryTime);
            }
        } catch (SQLException ex) {
            throw new DataAccessException("Failed to update subtask " + categoryTime, ex);
        }
    }

    public void delete(CategoryTime categoryTime) {
        if (categoryTime.getId() == null) {
            throw new IllegalArgumentException("CategoryTime has null ID: " + categoryTime);
        }

        try (var connection = dataSource.getConnection();
             var st = connection.prepareStatement("DELETE FROM CATEGORYTIME WHERE ID = ?")) {
            st.setLong(1, categoryTime.getId());

            int rowsDeleted = st.executeUpdate();
            if (rowsDeleted == 0) {
                throw new DataAccessException("Failed to delete non-existing categoryTime: " + categoryTime);
            }
        } catch (SQLException ex) {
            throw new DataAccessException("Failed to delete categoryTime " + categoryTime, ex);
        }
    }

    public CategoryTime findById(Long id) {
        if (id == null) {
            throw new IllegalArgumentException("Cannot look for categoryTime of null ID");
        }

        try (var connection = dataSource.getConnection();
             var st = connection.prepareStatement(
                     "SELECT ID, COMPLETION_DATE, HOURS_SPENT, TASK_ID, CATEGORY_ID FROM CATEGORYTIME WHERE ID = ?")) {
            st.setLong(1, id);

            try (var rs = st.executeQuery()) {
                if (rs.next()) {
                    var categoryTime = toEntity(rs);
                    if (rs.next()) {
                        throw new DataAccessException("Multiple categoryTimes with id " + id + " found");
                    }
                    return categoryTime;
                }
                return null;
            }
        } catch (SQLException ex) {
            throw new DataAccessException("Failed to load categoryTime with id " + id, ex);
        }
    }

    private CategoryTime toEntity(ResultSet rs) throws SQLException {
        var categoryTime = new CategoryTime(
                rs.getDate("COMPLETION_DATE").toLocalDate(),
                rs.getLong("HOURS_SPENT"),
                rs.getLong("TASK_ID")
        );
        categoryTime.setId(rs.getLong("ID"));
        return categoryTime;
    }
}
