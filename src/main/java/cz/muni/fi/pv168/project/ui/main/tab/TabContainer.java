package cz.muni.fi.pv168.project.ui.main.tab;

import cz.muni.fi.pv168.project.ui.main.panel.CardPanel;

import javax.swing.BorderFactory;
import javax.swing.JLabel;
import javax.swing.JTabbedPane;
import javax.swing.SwingConstants;
import javax.swing.border.EmptyBorder;
import javax.swing.border.EtchedBorder;

public class TabContainer {
    private final JTabbedPane tabbedPane;
    private final CardPanel toolbarPanel;

    public TabContainer(CardPanel toolbarPanel) {
        tabbedPane = new JTabbedPane();
        tabbedPane.setBorder(new EtchedBorder());
        this.toolbarPanel = toolbarPanel;
    }

    public JTabbedPane getTabbedPane() {
        return tabbedPane;
    }

    public void addTab(Tab<?> tab) {
        final var tabView = tab.getComponent();
        final var tabLabel = new JLabel(tab.getLabel(), SwingConstants.CENTER);
        final var toolbar = tab.getToolbar();

        tabLabel.setBorder(BorderFactory.createCompoundBorder(
                tabLabel.getBorder(), new EmptyBorder(0, 10, 0, 10))
        );

        tabbedPane.add(tab.getLabel(), tabView);
        toolbarPanel.addComponent(toolbar, tab.getLabel());
        tabbedPane.setTabComponentAt(tabbedPane.getTabCount() - 1, tabLabel);
    }

    public String getCurrentTabLabel() {
        return tabbedPane.getTitleAt(tabbedPane.getSelectedIndex());
    }
}
