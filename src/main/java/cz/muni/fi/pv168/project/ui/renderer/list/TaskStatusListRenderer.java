package cz.muni.fi.pv168.project.ui.renderer.list;

import cz.muni.fi.pv168.project.data.task.TaskStatus;
import cz.muni.fi.pv168.project.ui.main.panel.TaskStatusPanel;

import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.ListCellRenderer;
import java.awt.Component;
import java.util.function.Function;

public class TaskStatusListRenderer<T> implements ListCellRenderer<T> {

    private final InnerListRenderer<T> listRenderer;
    private final Function<T, TaskStatus> statusGetter;
    private final Function<T, Boolean> urgencyGetter;

    public TaskStatusListRenderer(
            Function<T, String> stringGetter, Function<T, TaskStatus> statusGetter, Function<T, Boolean> urgencyGetter, int fontStyle) {
        listRenderer = new InnerListRenderer<>(stringGetter, fontStyle);
        this.statusGetter = statusGetter;
        this.urgencyGetter = urgencyGetter;
    }

    @Override
    public Component getListCellRendererComponent(JList<? extends T> jList, T t, int i, boolean b, boolean b1) {
        JPanel panel = (JPanel) listRenderer.getListCellRendererComponent(jList, t, i, b, b1);
        panel.add(new TaskStatusPanel(statusGetter.apply(t), urgencyGetter.apply(t), false));
        return panel;
    }
}
