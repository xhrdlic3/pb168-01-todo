package cz.muni.fi.pv168.project.ui.renderer.statistics;

import javax.swing.JComponent;
import javax.swing.JTable;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.TableCellRenderer;
import java.awt.Component;

public abstract class AbstractStatisticsRenderer<T> implements TableCellRenderer {

    private final Class<T> elementType;
    private final DefaultTableCellRenderer tableCellRenderer = new DefaultTableCellRenderer();

    protected AbstractStatisticsRenderer(Class<T> elementType) {
        this.elementType = elementType;
    }

    @Override
    public Component getTableCellRendererComponent(
            JTable table, Object value,
            boolean isSelected, boolean hasFocus,
            int row, int column) {

        // reset foreground color to default
        tableCellRenderer.setForeground(null);
        var component = (JComponent) tableCellRenderer.getTableCellRendererComponent(
                table, value, isSelected, hasFocus, row, column);
        return modifyComponent(elementType.cast(value));
    }

    protected abstract JComponent modifyComponent(T value);
}
