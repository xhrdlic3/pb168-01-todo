package cz.muni.fi.pv168.project;

import cz.muni.fi.pv168.project.db.DaoHolder;
import cz.muni.fi.pv168.project.ui.main.MainWindow;
import org.apache.derby.jdbc.EmbeddedDataSource;

import javax.sql.DataSource;
import java.awt.*;

/**
 * The entry point of the application.
 */
public final class Main {

    private Main() {
        throw new AssertionError("This class is not intended for instantiation.");
    }

    public static void main(String[] args) {
        setLookAndFeel();

        var daoSupervisor = new DaoHolder(createDataSource());

        EventQueue.invokeLater(() -> new MainWindow(daoSupervisor));
    }

    public static void setLookAndFeel() {
        for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
            if ("GTK+".equals(info.getName()) || isOsWindows() && "Windows".equals(info.getName())) {
                try {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                } catch (Exception e) {
                    System.err.println("Unable to load Look&Feel " + info.getClassName());
                }
                break;
            }
        }
    }

    public static boolean isOsWindows() {
        return System.getProperty("os.name").toLowerCase().startsWith("win");
    }

    private static DataSource createDataSource() {
        String dbPath = System.getProperty("user.home") + "/pv168/db/0101todo/";

        EmbeddedDataSource dataSource = new EmbeddedDataSource();
        dataSource.setDatabaseName(dbPath);
        dataSource.setCreateDatabase("create");

        return dataSource;
    }
}
