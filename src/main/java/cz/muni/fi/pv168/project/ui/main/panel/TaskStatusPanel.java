package cz.muni.fi.pv168.project.ui.main.panel;

import cz.muni.fi.pv168.project.data.task.TaskStatus;
import cz.muni.fi.pv168.project.ui.resources.Icons;

import javax.swing.Box;
import javax.swing.JLabel;
import javax.swing.JPanel;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;

public class TaskStatusPanel extends JPanel {

    private final GridBagConstraints gbc;
    private final boolean useBiggerIcons;

    public TaskStatusPanel(TaskStatus status, boolean isTaskUrgent, boolean useBiggerIcons) {
        super(new GridBagLayout());
        this.useBiggerIcons = useBiggerIcons;

        this.setOpaque(false);
        gbc = new GridBagConstraints();
        gbc.fill = GridBagConstraints.BOTH;
        gbc.anchor = GridBagConstraints.NORTHWEST;
        gbc.weightx = 0;
        gbc.weighty = 1;

        setTaskStatus(status, isTaskUrgent);
    }

    public void setTaskStatus(TaskStatus status, boolean isTaskUrgent) {
        this.removeAll();

        gbc.gridy = 0;
        this.add(new JLabel(Icons.getTaskStatusIcon(status, useBiggerIcons)), gbc);

        if (isTaskUrgent && !status.equals(TaskStatus.FINISHED)) {
            gbc.gridy = 1;
            this.add(Box.createVerticalStrut(1), gbc);

            gbc.gridy = 2;
            this.add(new JLabel(Icons.getUrgencyIcon(useBiggerIcons)), gbc);
        }
    }
}
