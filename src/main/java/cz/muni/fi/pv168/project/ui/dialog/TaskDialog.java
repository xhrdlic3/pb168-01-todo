package cz.muni.fi.pv168.project.ui.dialog;

import cz.muni.fi.pv168.project.data.task.Task;
import cz.muni.fi.pv168.project.model.LocalDateModel;
import cz.muni.fi.pv168.project.ui.i18n.I18N;
import org.jdatepicker.DateModel;
import org.jdatepicker.JDatePicker;

import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.text.AbstractDocument;
import javax.swing.text.AttributeSet;
import javax.swing.text.BadLocationException;
import javax.swing.text.DocumentFilter;
import java.awt.BorderLayout;
import java.time.Duration;
import java.time.LocalDate;
import java.util.List;
import java.util.regex.Pattern;

public class TaskDialog extends AbstractLaidOutDialog<Task> {

    private Task entity;

    private final DateModel<LocalDate> dueTimeModel = new LocalDateModel();

    private final JTextField titleField = new JTextField();
    private final JTextArea descriptionField = new JTextArea(5, 0);
    private final JTextField estimatedTimeField = new IntegerTextField(0);
    private final JDatePicker datePicker = new JDatePicker(dueTimeModel);

    private static final I18N I18N = new I18N(TaskDialog.class);

    public TaskDialog() {
        descriptionField.setLineWrap(true);
        descriptionField.setFont(descriptionField.getFont().deriveFont(11f));

        addFields();
    }

    public TaskDialog(Task initialEntity) {
        this();

        setEntity(initialEntity);
    }

    protected void addFields() {
        addLabeledField(I18N.getString("labelTitle"), titleField, labelGbc, componentGbc, getRowThenIncrement());
        addLabeledField(I18N.getString("labelDescription"),
                new JScrollPane(descriptionField), labelGbc, componentGbc, getRowThenIncrement());
        addLabeledField(I18N.getString("labelEstTime"), createEstimatedTimePanel(), labelGbc, componentGbc, getRowThenIncrement());
        addLabeledField(I18N.getString("labelDueTime"), datePicker, labelGbc, componentGbc, getRowThenIncrement());
    }

    private JPanel createEstimatedTimePanel() {
        JPanel estimatePanel = new JPanel(new BorderLayout());

        estimatePanel.add(estimatedTimeField, BorderLayout.CENTER);
        estimatePanel.add(new JLabel(" " + I18N.getString("estimatedTimeUnit")), BorderLayout.EAST);

        return estimatePanel;
    }

    @Override
    Task getEntity() {
        if (entity != null) {
            entity.setTitle(titleField.getText());
            entity.setDescription(descriptionField.getText());
            entity.setEstimatedTime(Duration.ofHours(Long.parseLong(estimatedTimeField.getText())));
            entity.setDueDate((LocalDate) datePicker.getModel().getValue());

            return entity;
        }

        return Task.builder(
                        titleField.getText(), Long.parseLong(estimatedTimeField.getText()), (LocalDate) datePicker.getModel().getValue()
                )
                .setDescription(descriptionField.getText())
                .setCategories(List.of())
                .setSubTasks(List.of())
                .setDependencyTasks(List.of())
                .build();
    }

    public void setEntity(Task initialEntity) {
        entity = initialEntity;

        titleField.setText(initialEntity.getTitle());
        estimatedTimeField.setText(Long.toString(initialEntity.getEstimatedTime()));
        descriptionField.setText(initialEntity.getDescription());

        dueTimeModel.setValue(initialEntity.getDueDate());
    }

    private static final class IntegerTextField extends JTextField {

        private IntegerTextField(Integer value) {
            super(String.valueOf(value));

            ((AbstractDocument) this.getDocument()).setDocumentFilter(new DocumentFilter() {
                private final Pattern pattern = Pattern.compile("\\d*");

                @Override
                public void replace(FilterBypass fb, int offset, int length, String text, AttributeSet attrs)
                        throws BadLocationException {
                    if (!pattern.matcher(text).matches()) {
                        return;
                    }

                    super.replace(fb, offset, length, text, attrs);
                }
            });
        }
    }
}
