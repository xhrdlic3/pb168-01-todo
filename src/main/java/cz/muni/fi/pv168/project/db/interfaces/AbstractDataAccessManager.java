package cz.muni.fi.pv168.project.db.interfaces;

import cz.muni.fi.pv168.project.db.DataAccessException;

import java.sql.Connection;
import java.sql.SQLException;

public abstract class AbstractDataAccessManager {

    public abstract String getTableName();

    public abstract String getCreateTableStatement();

    public boolean tableDoesNotExist() {
        try (var connection = getConnection();
             var rs = connection.getMetaData().getTables(null, null, getTableName(), null)) {
            return !rs.next();
        } catch (SQLException ex) {
            throw new DataAccessException("Failed to detect if the table " + getTableName() + " exist", ex);
        }
    }

    public void createTable() {
        try (var connection = getConnection();
             var st = connection.createStatement()) {

            st.executeUpdate(getCreateTableStatement());
        } catch (SQLException ex) {
            throw new DataAccessException("Failed to create " + getTableName() + " table", ex);
        }
    }

    public void dropTable() {
        try (var connection = getConnection();
             var st = connection.createStatement()) {

            st.executeUpdate("DROP TABLE " + getTableName());
        } catch (SQLException ex) {
            throw new DataAccessException("Failed to drop " + getTableName() + " table", ex);
        }
    }

    protected abstract Connection getConnection() throws SQLException;
}
