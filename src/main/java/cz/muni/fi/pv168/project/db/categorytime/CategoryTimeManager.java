package cz.muni.fi.pv168.project.db.categorytime;

import cz.muni.fi.pv168.project.db.interfaces.AbstractDataAccessManager;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.SQLException;

public class CategoryTimeManager extends AbstractDataAccessManager {

    private final DataSource dataSource;

    public CategoryTimeManager(DataSource dataSource) {
        this.dataSource = dataSource;

        if (tableDoesNotExist()) {
            createTable();
        }
    }

    @Override
    public String getTableName() {
        return "CATEGORYTIME";
    }

    @Override
    public String getCreateTableStatement() {
        return "CREATE TABLE " + getTableName() + "(" +
                "ID BIGINT PRIMARY KEY GENERATED ALWAYS AS IDENTITY," +
                "CATEGORY_ID BIGINT NOT NULL REFERENCES CATEGORY(ID)," +
                "TASK_ID BIGINT," +
                "HOURS_SPENT BIGINT," +
                "COMPLETION_DATE DATE" +
                ")";
    }

    @Override
    public Connection getConnection() throws SQLException {
        return dataSource.getConnection();
    }
}
