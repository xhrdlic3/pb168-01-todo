package cz.muni.fi.pv168.project.ui.main.panel.listholder;

import cz.muni.fi.pv168.project.data.task.SubTask;
import cz.muni.fi.pv168.project.data.task.TaskStatus;
import cz.muni.fi.pv168.project.db.subtask.SubTaskDao;
import cz.muni.fi.pv168.project.model.db.SubTaskModel;
import cz.muni.fi.pv168.project.ui.action.AddAction;
import cz.muni.fi.pv168.project.ui.action.DeleteAction;
import cz.muni.fi.pv168.project.ui.action.EditAction;
import cz.muni.fi.pv168.project.ui.action.task.ToggleCompletionAction;
import cz.muni.fi.pv168.project.ui.dialog.factory.DialogFactory;
import cz.muni.fi.pv168.project.ui.dialog.factory.SubTaskDialogFactory;
import cz.muni.fi.pv168.project.ui.i18n.I18N;
import cz.muni.fi.pv168.project.ui.popup.PopupMenuFactory;
import cz.muni.fi.pv168.project.ui.renderer.list.TaskStatusListRenderer;
import cz.muni.fi.pv168.project.ui.resources.Icons;

import java.awt.Font;
import java.util.Optional;

public class SubTaskHolderPanel extends AbstractHolderPanel<SubTask> {

    private static final I18N I18N = new I18N(SubTaskHolderPanel.class);

    public SubTaskHolderPanel(SubTaskDao subTaskDao) {
        super(
                new SubTaskModel(subTaskDao), I18N.getString("panelName"),
                new TaskStatusListRenderer<>(SubTask::getTitle,
                        task -> task.isCompleted() ? TaskStatus.FINISHED : TaskStatus.PLANNED, task -> false, Font.PLAIN),
                Optional.of(SubTask::isCompleted)
        );

        PopupMenuFactory.builder()
                .addMenuItem(new ToggleCompletionAction<>(this, I18N.getString("completionToggle")).addUpdatablePanel(this))
                .addSeparator()
                .addMenuItem(new AddAction<>(this, Icons.ADD_TASK_ICON, I18N.getString("addAction")).addUpdatablePanel(this))
                .addMenuItem(new EditAction<>(this, Icons.EDIT_TASK_ICON, I18N.getString("editAction")).addUpdatablePanel(this))
                .addMenuItem(new DeleteAction<>(this, Icons.REMOVE_TASK_BIN_ICON, I18N.getString("removeAction")).addUpdatablePanel(this))
                .buildFor(this.getList());
    }

    @Override
    public DialogFactory<SubTask> getDialogFactory() {
        return new SubTaskDialogFactory();
    }
}
