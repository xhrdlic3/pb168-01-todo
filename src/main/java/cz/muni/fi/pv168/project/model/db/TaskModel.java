package cz.muni.fi.pv168.project.model.db;

import cz.muni.fi.pv168.project.data.task.Task;
import cz.muni.fi.pv168.project.db.DataAccessException;
import cz.muni.fi.pv168.project.db.interfaces.DataAccessObject;
import cz.muni.fi.pv168.project.ui.dialog.error.ErrorDialog;
import cz.muni.fi.pv168.project.ui.i18n.I18N;

import java.util.ArrayList;
import java.util.function.Predicate;
import java.util.logging.Level;
import java.util.logging.Logger;

public class TaskModel extends UpdatableDataModel<Task> {

    private static final I18N I18N = new I18N(TaskModel.class);
    private static final Logger LOG = Logger.getLogger(TaskModel.class.getName());

    public TaskModel(DataAccessObject<Task> dao) {
        super(dao);
    }

    public void updateFiltered(Predicate<Task> predicate) {
        clear();
        try {
            var tasks = new ArrayList<Task>();
            for (Task element : dataAccessObject.getAll()) {
                if (predicate.test(element)) {
                    tasks.add(element);
                }
            }
            addAll(tasks);
        } catch (DataAccessException e) {
            LOG.log(Level.SEVERE, "Error loading all tasks from database", e);
            ErrorDialog.show(I18N.getString("queryAllError"), e);
        }
    }
}
