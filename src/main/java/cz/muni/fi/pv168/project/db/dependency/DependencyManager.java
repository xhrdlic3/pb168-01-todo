package cz.muni.fi.pv168.project.db.dependency;

import cz.muni.fi.pv168.project.db.interfaces.AbstractDataAccessManager;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.SQLException;

public class DependencyManager extends AbstractDataAccessManager {

    private final DataSource dataSource;

    public DependencyManager(DataSource dataSource) {
        this.dataSource = dataSource;

        if (tableDoesNotExist()) {
            createTable();
        }
    }

    @Override
    public String getTableName() {
        return "TASK_DEPENDENCY";
    }

    @Override
    public String getCreateTableStatement() {
        return "CREATE TABLE " + getTableName() + "(" +
                "ID BIGINT PRIMARY KEY GENERATED ALWAYS AS IDENTITY," +
                "ID_SOURCE BIGINT REFERENCES TASK(ID)," +
                "ID_TARGET BIGINT REFERENCES TASK(ID)" +
                ")";
    }

    @Override
    public Connection getConnection() throws SQLException {
        return dataSource.getConnection();
    }
}
