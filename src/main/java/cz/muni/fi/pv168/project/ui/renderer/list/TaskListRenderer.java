package cz.muni.fi.pv168.project.ui.renderer.list;

import cz.muni.fi.pv168.project.data.task.Task;
import cz.muni.fi.pv168.project.data.task.TaskStatus;
import cz.muni.fi.pv168.project.ui.i18n.I18N;
import cz.muni.fi.pv168.project.ui.main.panel.TaskStatusPanel;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import javax.swing.border.EtchedBorder;
import java.awt.*;

public class TaskListRenderer implements ListCellRenderer<Task> {

    private final JLabel titleLabel;
    private final JLabel dueDateLabel;
    private final JLabel descriptionLabel;
    private final JLabel estimatedTimeLabel;
    private final TaskStatusPanel completionPanel;
    private final JPanel panel;

    private static final I18N I18N = new I18N(TaskListRenderer.class);

    private static final String TITLE_DATE_DELIMITER = I18N.getString("delimiter");

    public TaskListRenderer() {
        panel = new JPanel(new GridBagLayout());

        titleLabel = new JLabel();
        titleLabel.setFont(titleLabel.getFont().deriveFont(Font.BOLD));
        titleLabel.setBorder(new EmptyBorder(0, 0, 0, 0));

        dueDateLabel = new JLabel();
        dueDateLabel.setFont(dueDateLabel.getFont().deriveFont(Font.BOLD));

        JPanel headingPanel = new JPanel(new FlowLayout(FlowLayout.LEFT, 0, 0));
        headingPanel.setOpaque(false);
        headingPanel.add(titleLabel);
        headingPanel.add(Box.createHorizontalStrut(5));
        headingPanel.add(new JLabel(TITLE_DATE_DELIMITER));
        headingPanel.add(Box.createHorizontalStrut(5));
        headingPanel.add(dueDateLabel);

        estimatedTimeLabel = new JLabel();
        estimatedTimeLabel.setFont(estimatedTimeLabel.getFont().deriveFont(Font.BOLD));

        descriptionLabel = new JLabel();
        descriptionLabel.setFont(descriptionLabel.getFont().deriveFont(Font.ITALIC));

        completionPanel = new TaskStatusPanel(TaskStatus.PLANNED, false, true);

        GridBagConstraints gbc = new GridBagConstraints();
        gbc.anchor = GridBagConstraints.NORTHWEST;
        gbc.fill = GridBagConstraints.BOTH;
        gbc.insets = new Insets(3, 3, 3, 3);
        gbc.weightx = 1;

        panel.add(headingPanel, gbc);

        gbc.gridy = 1;
        panel.add(estimatedTimeLabel, gbc);

        gbc.gridy = 2;
        panel.add(descriptionLabel, gbc);

        gbc.gridx = 1;
        gbc.gridy = 0;
        gbc.weightx = 0;
        gbc.gridheight = 3;

        panel.add(completionPanel, gbc);

        panel.setBorder(BorderFactory.createCompoundBorder(
                new EtchedBorder(EtchedBorder.LOWERED),
                new EmptyBorder(5, 5, 5, 5)
        ));
    }

    @Override
    public Component getListCellRendererComponent(
            JList<? extends Task> list, Task task, int index, boolean isSelected, boolean cellHasFocus
    ) {
        titleLabel.setText(stringEllipsis(task.getTitle(), 30));
        dueDateLabel.setText(task.getDueDateString());

        String description = task.getDescription();
        if (description == null || description.isEmpty()) {
            descriptionLabel.setVisible(false);
        } else {
            descriptionLabel.setVisible(true);
            descriptionLabel.setText(stringEllipsis(
                    description,
                    (titleLabel.getText() + "  " + TITLE_DATE_DELIMITER + "  " + dueDateLabel.getText()).length()
            ));
        }

        estimatedTimeLabel.setText(I18N.getFormattedMessage("timeFormat", task.getEstimatedTime()));

        completionPanel.setTaskStatus(task.getTaskStatus(), task.isUrgent());
        completionPanel.repaint();

        if (isSelected) {
            panel.setBackground(list.getSelectionBackground());
            panel.setForeground(list.getSelectionForeground());
        } else {
            panel.setBackground(list.getBackground());
            panel.setForeground(list.getForeground());
        }
        return panel;
    }

    public String stringEllipsis(String string, int maxLength) {
        int stringLength = string.length();

        if (stringLength <= maxLength) {
            return string;
        }

        return string.substring(0, maxLength).trim() + "…";
    }
}
