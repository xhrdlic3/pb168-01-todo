package cz.muni.fi.pv168.project.ui.action.task;

import cz.muni.fi.pv168.project.data.task.Task;
import cz.muni.fi.pv168.project.ui.action.AbstractAction;
import cz.muni.fi.pv168.project.ui.i18n.I18N;
import cz.muni.fi.pv168.project.ui.main.tab.ListHolder;

import javax.swing.JList;
import javax.swing.KeyStroke;
import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;
import java.util.List;

public final class GoToAction extends AbstractAction<Task> {

    private final JList<Task> destinationList;
    private static final I18N I18N = new I18N(GoToAction.class);

    public GoToAction(ListHolder<Task> sourceListHolder, JList<Task> destinationList, String description) {
        super(sourceListHolder, selectedRowsCount -> selectedRowsCount == 1);
        this.destinationList = destinationList;

        putValue(NAME, I18N.getString("goTo"));
        putValue(SHORT_DESCRIPTION, description);
        putValue(MNEMONIC_KEY, KeyEvent.VK_G);
        putValue(ACCELERATOR_KEY, KeyStroke.getKeyStroke("ctrl G"));
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        var sourceList = getList();

        List<?> selectedRows = sourceList.getSelectedValuesList();
        if (selectedRows.size() != 1) {
            throw new IllegalStateException("Invalid selected rows count (must be 1): " + selectedRows.size());
        }

        destinationList.setSelectedValue(sourceList.getSelectedValue(), true);
    }
}
