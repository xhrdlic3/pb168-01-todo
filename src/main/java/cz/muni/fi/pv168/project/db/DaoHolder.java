package cz.muni.fi.pv168.project.db;

import cz.muni.fi.pv168.project.db.category.CategoryDao;
import cz.muni.fi.pv168.project.db.category.CategoryManager;
import cz.muni.fi.pv168.project.db.categorytime.CategoryTimeDao;
import cz.muni.fi.pv168.project.db.categorytime.CategoryTimeManager;
import cz.muni.fi.pv168.project.db.dependency.DependencyDao;
import cz.muni.fi.pv168.project.db.dependency.DependencyManager;
import cz.muni.fi.pv168.project.db.subtask.SubTaskDao;
import cz.muni.fi.pv168.project.db.subtask.SubTaskManager;
import cz.muni.fi.pv168.project.db.task.TaskDao;
import cz.muni.fi.pv168.project.db.task.TaskManager;
import cz.muni.fi.pv168.project.db.taskcategory.TaskCategoryDao;
import cz.muni.fi.pv168.project.db.taskcategory.TaskCategoryManager;

import javax.sql.DataSource;

public class DaoHolder {

    private final SubTaskDao subTaskDao;
    private final DependencyDao dependencyDao;
    private final CategoryDao categoryDao;
    private final TaskCategoryDao taskCategoryDao;
    private final TaskDao taskDao;

    public DaoHolder(DataSource dataSource) {
        new TaskManager(dataSource);
        new SubTaskManager(dataSource);
        new DependencyManager(dataSource);

        new CategoryManager(dataSource);
        new TaskCategoryManager(dataSource);
        new CategoryTimeManager(dataSource);

        var categoryTimeDao = new CategoryTimeDao(dataSource);
        categoryDao = new CategoryDao(dataSource, categoryTimeDao);

        subTaskDao = new SubTaskDao(dataSource);
        dependencyDao = new DependencyDao(dataSource);
        taskCategoryDao = new TaskCategoryDao(dataSource);
        taskDao = new TaskDao(dataSource, subTaskDao, dependencyDao, taskCategoryDao, categoryDao, categoryTimeDao);
    }

    public SubTaskDao getSubTaskDao() {
        return subTaskDao;
    }

    public DependencyDao getDependencyDao() {
        return dependencyDao;
    }

    public CategoryDao getCategoryDao() {
        return categoryDao;
    }

    public TaskCategoryDao getTaskCategoryDao() {
        return taskCategoryDao;
    }

    public TaskDao getTaskDao() {
        return taskDao;
    }
}
