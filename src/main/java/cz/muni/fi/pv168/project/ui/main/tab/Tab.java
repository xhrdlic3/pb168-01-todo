package cz.muni.fi.pv168.project.ui.main.tab;

import javax.swing.JComponent;
import javax.swing.JToolBar;

public interface Tab<T> extends ListHolder<T> {

    String getLabel();

    JComponent getComponent();

    JToolBar getToolbar();
}
