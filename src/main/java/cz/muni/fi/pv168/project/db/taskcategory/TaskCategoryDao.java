package cz.muni.fi.pv168.project.db.taskcategory;

import cz.muni.fi.pv168.project.db.DataAccessException;

import javax.sql.DataSource;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class TaskCategoryDao {

    private final DataSource dataSource;

    public TaskCategoryDao(DataSource dataSource) {
        this.dataSource = dataSource;
    }

    public List<Long> getAllCategoryIdsForTaskId(Long taskId) {
        try (var connection = dataSource.getConnection();
             var st = connection.prepareStatement("SELECT ID_CATEGORY FROM TASK_CATEGORY WHERE ID_TASK = ?")) {
            st.setLong(1, taskId);

            List<Long> categoryIds = new ArrayList<>();
            try (var rs = st.executeQuery()) {
                while (rs.next()) {
                    categoryIds.add(rs.getLong("ID_CATEGORY"));
                }

                return categoryIds;
            }
        } catch (SQLException ex) {
            throw new DataAccessException("Failed to load dependencies with task id " + taskId, ex);
        }
    }

    public void addCategoryAssociationFor(Long taskId, Long categoryId) {
        if (taskId == null) {
            throw new IllegalArgumentException("Task has null ID");
        }
        if (categoryId == null) {
            throw new IllegalArgumentException("Category has null ID");
        }

        try (var connection = dataSource.getConnection();
             var st = connection.prepareStatement(
                     "INSERT INTO TASK_CATEGORY (ID_TASK, ID_CATEGORY) VALUES (?, ?)")) {
            st.setLong(1, taskId);
            st.setLong(2, categoryId);

            st.executeUpdate();
        } catch (SQLException ex) {
            throw new DataAccessException("Failed to store task-category between IDs: " + taskId + "-" + categoryId, ex);
        }
    }

    public void deleteCategoryAssociationFor(Long taskId, Long categoryId) {
        if (taskId == null) {
            throw new IllegalArgumentException("Task has null ID");
        }
        if (categoryId == null) {
            throw new IllegalArgumentException("Category has null ID");
        }

        try (var connection = dataSource.getConnection();
             var st = connection.prepareStatement("DELETE FROM TASK_CATEGORY WHERE ID_TASK = ? AND ID_CATEGORY = ?")) {
            st.setLong(1, taskId);
            st.setLong(2, categoryId);

            int rowsDeleted = st.executeUpdate();
            if (rowsDeleted == 0) {
                throw new DataAccessException("Failed to delete non-existing task-category between IDs: " + taskId + "-" + categoryId);
            }
        } catch (SQLException ex) {
            throw new DataAccessException("Failed to delete task-category between IDs: " + taskId + "-" + categoryId, ex);
        }
    }

    public void deleteAllCategoryAssociationsForTask(Long sourceTaskId) {
        var ids = getAllCategoryIdsForTaskId(sourceTaskId);
        for (var id : ids) {
            deleteCategoryAssociationFor(sourceTaskId, id);
        }
    }
}
