package cz.muni.fi.pv168.project.ui.main.panel.listholder;

import cz.muni.fi.pv168.project.data.task.Task;
import cz.muni.fi.pv168.project.model.db.AbstractTaskPropertyListDataModel;
import cz.muni.fi.pv168.project.ui.action.UpdatablePanel;
import cz.muni.fi.pv168.project.ui.main.panel.CounterPanel;
import cz.muni.fi.pv168.project.ui.main.tab.ListHolder;
import org.jetbrains.annotations.Nullable;

import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.ListCellRenderer;
import javax.swing.border.TitledBorder;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.util.Optional;
import java.util.function.Predicate;

public abstract class AbstractHolderPanel<T> implements ListHolder<T>, UpdatablePanel {

    protected final JList<T> jList;
    private final JPanel panel;
    private Optional<CounterPanel<T>> counterPanel = Optional.empty();

    private final AbstractTaskPropertyListDataModel<T> dataModel;

    protected AbstractHolderPanel(AbstractTaskPropertyListDataModel<T> model, String panelName,
                                  ListCellRenderer<T> renderer, Optional<Predicate<T>> maybeCounter) {
        this.dataModel = model;
        jList = new JList<T>();

        jList.setCellRenderer(renderer);
        jList.setEnabled(true);
        jList.setOpaque(false);
        jList.setModel(model);

        GridBagConstraints gbc = new GridBagConstraints();
        gbc.fill = GridBagConstraints.BOTH;
        gbc.anchor = GridBagConstraints.NORTHWEST;
        gbc.insets = new Insets(5, 5, 5, 5);
        gbc.weightx = 1;
        gbc.weighty = 1;
        gbc.gridy = 0;

        panel = new JPanel(new GridBagLayout());
        panel.add(new JScrollPane(jList), gbc);
        panel.setBorder(new TitledBorder(panelName));

        if (maybeCounter.isPresent()) {
            gbc.weighty = 0;
            gbc.gridy = 1;
            counterPanel = Optional.of(new CounterPanel<>(jList, maybeCounter.get()));
            panel.add(counterPanel.get(), gbc);
        }
    }

    public JPanel getPanel() {
        return panel;
    }

    @Override
    public JList<T> getList() {
        return jList;
    }

    @Override
    public AbstractTaskPropertyListDataModel<T> getModel() {
        return dataModel;
    }


    @Override
    public void updateFrom(@Nullable Task task) {
        if (task != null) {
            dataModel.setParentTask(task);
            dataModel.clear();
            dataModel.addAll(dataModel.fetchAll());
        }
        counterPanel.ifPresent(CounterPanel::updateCounter);
    }
}
