package cz.muni.fi.pv168.project.ui.main.view;

import cz.muni.fi.pv168.project.data.task.Task;
import cz.muni.fi.pv168.project.data.task.TaskStatus;
import cz.muni.fi.pv168.project.db.DaoHolder;
import cz.muni.fi.pv168.project.db.category.CategoryDao;
import cz.muni.fi.pv168.project.model.db.TaskModel;
import cz.muni.fi.pv168.project.ui.action.AddAction;
import cz.muni.fi.pv168.project.ui.action.DeleteAction;
import cz.muni.fi.pv168.project.ui.action.EditAction;
import cz.muni.fi.pv168.project.ui.action.TabActions;
import cz.muni.fi.pv168.project.ui.action.UpdatablePanel;
import cz.muni.fi.pv168.project.ui.action.task.ToggleCompletionAction;
import cz.muni.fi.pv168.project.ui.dialog.factory.DialogFactory;
import cz.muni.fi.pv168.project.ui.dialog.factory.TaskDialogFactory;
import cz.muni.fi.pv168.project.ui.i18n.I18N;
import cz.muni.fi.pv168.project.ui.main.panel.CardPanel;
import cz.muni.fi.pv168.project.ui.main.panel.TaskPanel;
import cz.muni.fi.pv168.project.ui.main.panel.WelcomePanel;
import cz.muni.fi.pv168.project.ui.main.panel.listholder.CategoryHolderPanel;
import cz.muni.fi.pv168.project.ui.main.panel.listholder.DependencyHolderPanel;
import cz.muni.fi.pv168.project.ui.main.panel.listholder.SubTaskHolderPanel;
import cz.muni.fi.pv168.project.ui.main.tab.Tab;
import cz.muni.fi.pv168.project.ui.main.toolbar.TasksToolBar;
import cz.muni.fi.pv168.project.ui.popup.PopupMenuFactory;
import cz.muni.fi.pv168.project.ui.renderer.list.TaskListRenderer;
import cz.muni.fi.pv168.project.ui.resources.Icons;

import javax.swing.JComponent;
import javax.swing.JList;
import javax.swing.JScrollPane;
import javax.swing.JSplitPane;
import javax.swing.JToolBar;
import javax.swing.border.EtchedBorder;
import java.awt.event.ActionListener;
import java.util.function.Predicate;

public class TaskView implements Tab<Task> {

    private final TaskModel taskModel;
    private final CategoryHolderPanel categoryHolder;
    private final SubTaskHolderPanel subTaskHolder;
    private final DependencyHolderPanel dependencyHolder;

    private final DialogFactory<Task> dialogFactory;

    private final TasksToolBar toolBar;
    private final TabActions tabActions;

    private final JList<Task> headerList;
    private final JSplitPane splitPane;
    private final TaskPanel taskPanel;
    private final CardPanel cardPanel;

    private static final I18N I18N = new I18N(TaskView.class);

    public static final String WELCOME_CARD = I18N.getString("welcomeCard");
    public static final String TASK_CARD = I18N.getString("taskCard");

    public TaskView(DaoHolder daoHolder) {
        taskModel = new TaskModel(daoHolder.getTaskDao());

        splitPane = new JSplitPane(JSplitPane.HORIZONTAL_SPLIT);
        dialogFactory = new TaskDialogFactory();

        headerList = new JList<>(taskModel);
        headerList.setCellRenderer(new TaskListRenderer());
        headerList.setBorder(new EtchedBorder());

        categoryHolder = new CategoryHolderPanel(daoHolder.getTaskCategoryDao(), daoHolder.getCategoryDao());
        subTaskHolder = new SubTaskHolderPanel(daoHolder.getSubTaskDao());
        dependencyHolder = new DependencyHolderPanel(headerList, daoHolder.getDependencyDao(), daoHolder.getTaskDao());

        taskPanel = new TaskPanel(categoryHolder, subTaskHolder, dependencyHolder);
        cardPanel = new CardPanel()
                .addComponent(new WelcomePanel().getPanel(), WELCOME_CARD)
                .addComponent(taskPanel, TASK_CARD);

        tabActions = TabActions.builder()
                .addLeftSideAction(new AddAction<>(this, Icons.ADD_TASK_ICON, I18N.getString("addActionDesc")))
                .addLeftSideAction(new EditAction<>(
                        this, Icons.EDIT_TASK_ICON, I18N.getString("editActionDesc"), headerList::getSelectedValue
                ).addUpdatablePanel(taskPanel))
                //TODO: needs refactor with a variable holding selectedCount of tasks to be deleted for a proper localisation
                .addLeftSideAction(new DeleteAction<>(this, Icons.REMOVE_TASK_BIN_ICON, I18N.getFormattedMessage("deleteActionDesc"))
                        .addEnabledCondition(selectedCount -> daoHolder
                                .getTaskDao()
                                .getAll()
                                .stream()
                                .noneMatch(task -> task.getDependencyTasks().contains(getList().getSelectedValue())))
                ).build();
        toolBar = new TasksToolBar(tabActions);

        createPopupMenu(daoHolder.getCategoryDao());
        headerList.addListSelectionListener(e -> {
            if (headerList.getSelectedIndex() != -1) {
                cardPanel.showComponent(TASK_CARD);
                update();
            } else {
                cardPanel.showComponent(WELCOME_CARD);
            }
        });

        handleToolBar();

        splitPane.setLeftComponent(new JScrollPane(headerList));
        splitPane.setRightComponent(cardPanel.getContainer());
        splitPane.setDividerLocation(0.33);
        splitPane.setBorder(new EtchedBorder());
    }

    private void handleToolBar() {
        ActionListener listener = actionEvent -> {
            Predicate<Task> keepTask = task ->
                    !toolBar.isUrgentSelected() || task.shouldShowUrgency();
            var selectedStatus = toolBar.getSelectedStatus();

            if (selectedStatus != null) {
                keepTask = keepTask.and(task -> task.getTaskStatus().equals(selectedStatus));
            }

            taskModel.updateFiltered(keepTask);
        };

        toolBar.addTodayActionListener(listener);
        toolBar.addStatusActionListener(listener);
    }

    private void update() {
        var task = headerList.getSelectedValue();

        taskPanel.updateFrom(task);
        categoryHolder.updateFrom(task);
        subTaskHolder.updateFrom(task);
        dependencyHolder.updateFrom(task);
    }

    private void createPopupMenu(CategoryDao categoryDao) {
        PopupMenuFactory.builder()
                .addMenuItems(tabActions.getLeftSideActions())
                .addMenuItem(new ToggleCompletionAction<>(this, I18N.getString("actionToggle"), headerList::getSelectedValue)
                        .addUpdatablePanel(new UpdatablePanel() {
                            @Override
                            public void updateFrom(Task task) {
                                var selectedTask = headerList.getSelectedValue();

                                selectedTask.getCategories().forEach(category -> {
                                    if (selectedTask.getTaskStatus().equals(TaskStatus.FINISHED)) {
                                        category.addTime(selectedTask.getEstimatedTime(), selectedTask.getId());
                                        categoryDao.update(category);
                                    } else if (selectedTask.getTaskStatus().equals(TaskStatus.PLANNED)) {
                                        category.subtractTime(selectedTask.getId());
                                        categoryDao.update(category);
                                    }
                                });
                            }
                        }))
                .buildFor(headerList);
    }

    @Override
    public String getLabel() {
        return I18N.getString("label");
    }

    @Override
    public JComponent getComponent() {
        return splitPane;
    }

    @Override
    public JToolBar getToolbar() {
        return toolBar.getToolBar();
    }

    @Override
    public JList<Task> getList() {
        return headerList;
    }

    @Override
    public DialogFactory<Task> getDialogFactory() {
        return dialogFactory;
    }

    @Override
    public TaskModel getModel() {
        return taskModel;
    }
}
