package cz.muni.fi.pv168.project.db.subtask;

import cz.muni.fi.pv168.project.db.interfaces.AbstractDataAccessManager;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.SQLException;

public class SubTaskManager extends AbstractDataAccessManager {

    private final DataSource dataSource;

    public SubTaskManager(DataSource dataSource) {
        this.dataSource = dataSource;

        if (tableDoesNotExist()) {
            createTable();
        }
    }

    @Override
    public String getTableName() {
        return "SUBTASK";
    }

    @Override
    public String getCreateTableStatement() {
        return "CREATE TABLE " + getTableName() + "(" +
                "ID BIGINT PRIMARY KEY GENERATED ALWAYS AS IDENTITY," +
                "TASK_ID BIGINT NOT NULL REFERENCES TASK(ID)," +
                "TITLE VARCHAR(256)," +
                "IS_COMPLETED BOOLEAN" +
                ")";
    }

    @Override
    public Connection getConnection() throws SQLException {
        return dataSource.getConnection();
    }
}
