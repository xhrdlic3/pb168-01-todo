package cz.muni.fi.pv168.project.data.task;

public interface Completable {

    String getTitle();

    boolean isCompleted();

    void toggleIsCompleted();
}
