package cz.muni.fi.pv168.project.db.category;

import cz.muni.fi.pv168.project.db.interfaces.AbstractDataAccessManager;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.SQLException;

public class CategoryManager extends AbstractDataAccessManager {

    private final DataSource dataSource;

    public CategoryManager(DataSource dataSource) {
        this.dataSource = dataSource;

        if (tableDoesNotExist()) {
            createTable();
        }
    }

    @Override
    public Connection getConnection() throws SQLException {
        return dataSource.getConnection();
    }

    @Override
    public String getTableName() {
        return "CATEGORY";
    }

    @Override
    public String getCreateTableStatement() {
        return "CREATE TABLE " + getTableName() + "(" +
                "ID BIGINT PRIMARY KEY GENERATED ALWAYS AS IDENTITY," +
                "COLOR INT," +
                "TITLE VARCHAR(256)" +
                ")";
    }
}
