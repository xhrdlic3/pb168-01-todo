package cz.muni.fi.pv168.project.model.db;

import cz.muni.fi.pv168.project.data.task.Task;
import cz.muni.fi.pv168.project.db.dependency.DependencyDao;

public class TaskDependencyModel extends AbstractTaskPropertyListDataModel<Task> {

    private final DependencyDao dataAccessObject;

    public TaskDependencyModel(DependencyDao dao) {
        super(Task::getDependencyTasks);
        this.dataAccessObject = dao;
    }

    @Override
    public void delete(Task entity) {
        dataAccessObject.deleteDependencyAssociationFor(parentTask.getId(), entity.getId());
        parentTask.removeDependencyTask(entity);
    }

    @Override
    public void add(Task entity) {
        dataAccessObject.addDependencyAssociationFor(parentTask.getId(), entity.getId());
        parentTask.addDependencyTask(entity);
    }

    @Override
    public void update(Task entity) {
        throw new UnsupportedOperationException();
    }
}
