package cz.muni.fi.pv168.project.ui.dialog.factory;

import cz.muni.fi.pv168.project.data.category.Category;
import cz.muni.fi.pv168.project.ui.dialog.AbstractLaidOutDialog;
import cz.muni.fi.pv168.project.ui.dialog.CategoryDialog;

public class CategoryDialogFactory implements DialogFactory<Category> {

    @Override
    public AbstractLaidOutDialog<Category> newEditDialog(Category entity) {
        return new CategoryDialog(entity);
    }

    @Override
    public AbstractLaidOutDialog<Category> newAddDialog() {
        return new CategoryDialog();
    }
}
