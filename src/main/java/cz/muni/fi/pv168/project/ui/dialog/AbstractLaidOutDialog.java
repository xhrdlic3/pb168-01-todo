package cz.muni.fi.pv168.project.ui.dialog;

import java.awt.GridBagConstraints;
import java.awt.Insets;

public abstract class AbstractLaidOutDialog<E> extends AbstractEntityDialog<E> {

    protected final GridBagConstraints labelGbc;
    protected final GridBagConstraints componentGbc;
    protected final GridBagConstraints buttonGbc;
    protected final GridBagConstraints middleComponent;

    protected int rowCounter = 0;

    public AbstractLaidOutDialog() {
        labelGbc = new GridBagConstraints();
        labelGbc.fill = GridBagConstraints.HORIZONTAL;
        labelGbc.insets = new Insets(3, 3, 3, 3);
        labelGbc.weightx = 0;
        labelGbc.gridx = 0;

        componentGbc = new GridBagConstraints();
        componentGbc.insets = new Insets(3, 3, 3, 3);
        componentGbc.fill = GridBagConstraints.HORIZONTAL;
        componentGbc.gridwidth = 2;
        componentGbc.weightx = 1;
        componentGbc.gridx = 1;

        buttonGbc = (GridBagConstraints) labelGbc.clone();
        buttonGbc.gridx = 2;

        middleComponent = (GridBagConstraints) componentGbc.clone();
        middleComponent.gridwidth = 1;
    }

    protected final int getRow() {
        return rowCounter;
    }

    protected final int getRowThenIncrement() {
        int row = rowCounter;
        rowCounter++;
        return row;
    }
}
