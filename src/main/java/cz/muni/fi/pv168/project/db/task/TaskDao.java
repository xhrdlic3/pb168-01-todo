package cz.muni.fi.pv168.project.db.task;

import cz.muni.fi.pv168.project.data.category.Category;
import cz.muni.fi.pv168.project.data.task.Task;
import cz.muni.fi.pv168.project.data.task.TaskStatus;
import cz.muni.fi.pv168.project.db.DataAccessException;
import cz.muni.fi.pv168.project.db.category.CategoryDao;
import cz.muni.fi.pv168.project.db.categorytime.CategoryTimeDao;
import cz.muni.fi.pv168.project.db.dependency.DependencyDao;
import cz.muni.fi.pv168.project.db.interfaces.DataAccessObject;
import cz.muni.fi.pv168.project.db.subtask.SubTaskDao;
import cz.muni.fi.pv168.project.db.taskcategory.TaskCategoryDao;

import javax.sql.DataSource;
import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Types;
import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

public class TaskDao implements DataAccessObject<Task> {

    private final DataSource dataSource;
    private final SubTaskDao subTaskDao;
    private final DependencyDao dependencyDao;
    private final TaskCategoryDao taskCategoryDao;
    private final CategoryDao categoryDao;
    private final CategoryTimeDao categoryTimeDao;

    public TaskDao(
            DataSource dataSource,
            SubTaskDao subTaskDao,
            DependencyDao dependencyDao,
            TaskCategoryDao taskCategoryDao,
            CategoryDao categoryDao,
            CategoryTimeDao categoryTimeDao
    ) {
        this.dataSource = dataSource;
        this.subTaskDao = subTaskDao;
        this.dependencyDao = dependencyDao;
        this.taskCategoryDao = taskCategoryDao;
        this.categoryDao = categoryDao;
        this.categoryTimeDao = categoryTimeDao;
    }

    @Override
    public void add(Task entity) throws DataAccessException {
        if (entity.getId() != null) {
            throw new IllegalArgumentException("Task already has ID: " + entity);
        }

        try (var connection = dataSource.getConnection();
             var st = connection.prepareStatement(
                     "INSERT INTO TASK (TITLE, DUE_DATE, ESTIMATED_TIME, DESCRIPTION, TASK_STATUS) VALUES (?, ?, ?, ?, ?)",
                     Statement.RETURN_GENERATED_KEYS)) {
            st.setString(1, entity.getTitle());
            st.setDate(2, Date.valueOf(entity.getDueDate()));
            st.setLong(3, entity.getEstimatedTime());

            if (entity.getDescription() == null) {
                st.setNull(4, Types.VARCHAR);
            } else {
                st.setString(4, entity.getDescription());
            }
            st.setString(5, entity.getTaskStatus().upperCaseString());

            st.executeUpdate();

            try (var rs = st.getGeneratedKeys()) {
                if (rs.getMetaData().getColumnCount() != 1) {
                    throw new DataAccessException("Failed to fetch generated key: compound key returned for task: " + entity);
                }
                if (rs.next()) {
                    entity.setId(rs.getLong(1));
                } else {
                    throw new DataAccessException("Failed to fetch generated key: no key returned for task: " + entity);
                }
                if (rs.next()) {
                    throw new DataAccessException("Failed to fetch generated key: multiple keys returned for task: " + entity);
                }
            }

            addTaskFieldsToDaos(entity);
        } catch (SQLException ex) {
            throw new DataAccessException("Failed to store task " + entity, ex);
        }
    }

    @Override
    public Collection<Task> getAll() throws DataAccessException {
        try (var connection = dataSource.getConnection();
             var st = connection.prepareStatement("SELECT ID, TITLE, DUE_DATE, ESTIMATED_TIME, DESCRIPTION, TASK_STATUS FROM TASK")) {
            var taskMap = new LinkedHashMap<Long, Task>();
            try (var rs = st.executeQuery()) {
                while (rs.next()) {
                    Task task = toEntity(rs);
                    taskMap.put(task.getId(), task);
                }

                var categoryMap = new LinkedHashMap<Long, Category>();
                categoryDao.getAll().forEach(category -> {
                    categoryMap.put(category.getId(), category);
                });

                taskMap.forEach((id, task) -> {
                    task.setDependencyTasks(
                            dependencyDao.getAllDependencyIdsForTaskId(id).stream().map(taskMap::get).collect(Collectors.toList())
                    );
                    task.setCategories(
                            taskCategoryDao.getAllCategoryIdsForTaskId(id).stream().map(categoryMap::get).collect(Collectors.toList())
                    );
                });
            }
            return new LinkedList<>(taskMap.values());
        } catch (SQLException ex) {
            throw new DataAccessException("");
        }
    }

    @Override
    public Task fetch(Long id) {
        try (var connection = dataSource.getConnection();
             var st = connection.prepareStatement(
                     "SELECT ID, TITLE, DUE_DATE, ESTIMATED_TIME, DESCRIPTION, TASK_STATUS FROM TASK WHERE ID = ?")) {
            st.setLong(1, id);

            try (var rs = st.executeQuery()) {
                if (rs.next()) {
                    var task = toEntity(rs);
                    if (rs.next()) {
                        throw new DataAccessException("Multiple tasks with id " + id + " found");
                    }
                    return task;
                }
                return null;
            }
        } catch (SQLException ex) {
            throw new DataAccessException("Failed to load task with id " + id, ex);
        }
    }

    private Task toEntity(ResultSet rs) throws DataAccessException, SQLException {
        var taskId = rs.getLong("ID");

        var taskBuilder = Task.builder(
                        rs.getString("TITLE"),
                        rs.getLong("ESTIMATED_TIME"),
                        rs.getDate("DUE_DATE").toLocalDate())
                .setSubTasks(subTaskDao.getAllSubTasksForTask(taskId))
                .setDependencyTasks(List.of())
                .setCategories(List.of());
        var description = rs.getString("DESCRIPTION");
        if (!rs.wasNull()) {
            taskBuilder.setDescription(description);
        }

        var task = taskBuilder.build();
        task.setId(taskId);
        task.setTaskStatus(TaskStatus.valueOf(rs.getString("TASK_STATUS")));

        return task;
    }

    @Override
    public void update(Task entity) throws DataAccessException {
        if (entity.getId() == null) {
            throw new IllegalArgumentException("Task has null ID");
        }

        try (var connection = dataSource.getConnection();
             var st = connection.prepareStatement(
                     "UPDATE TASK SET TITLE = ?, DUE_DATE = ?, ESTIMATED_TIME = ?, DESCRIPTION = ?, TASK_STATUS = ? WHERE ID = ?")) {
            st.setString(1, entity.getTitle());
            st.setDate(2, Date.valueOf(entity.getDueDate()));
            st.setLong(3, entity.getEstimatedTime());
            if (entity.getDescription() == null) {
                st.setNull(4, Types.VARCHAR);
            } else {
                st.setString(4, entity.getDescription());
            }
            st.setString(5, entity.getTaskStatus().upperCaseString());
            st.setLong(6, entity.getId());

            int rowsUpdated = st.executeUpdate();
            if (rowsUpdated == 0) {
                throw new DataAccessException("Failed to update non-existing task: " + entity);
            }

            // cleanup previous
            subTaskDao.deleteAllSubTasksForTask(entity.getId());
            dependencyDao.deleteAllDependencyAssociationsForTask(entity.getId());
            taskCategoryDao.deleteAllCategoryAssociationsForTask(entity.getId());
            var timeSpents = categoryTimeDao.getAllCategoryTimesForTask(entity.getId());
            for (var timeSpent : timeSpents) {
                timeSpent.setHoursSpent(entity.getEstimatedTime());
                categoryTimeDao.update(timeSpent);
            }

            // add current
            addTaskFieldsToDaos(entity);
        } catch (SQLException ex) {
            throw new DataAccessException("Failed to update task " + entity, ex);
        }
    }

    private void addTaskFieldsToDaos(Task entity) {
        for (var subtask : entity.getSubTasks()) {
            subtask.setId(null);
            subTaskDao.addSubTaskForTask(entity.getId(), subtask);
        }
        for (var dependency : entity.getDependencyTasks()) {
            dependencyDao.addDependencyAssociationFor(entity.getId(), dependency.getId());
        }
        for (var category : entity.getCategories()) {
            taskCategoryDao.addCategoryAssociationFor(entity.getId(), category.getId());
        }
    }

    @Override
    public void delete(Task entity) throws DataAccessException {
        if (entity.getId() == null) {
            throw new IllegalArgumentException("Task has null ID: " + entity);
        }

        subTaskDao.deleteAllSubTasksForTask(entity.getId());
        dependencyDao.deleteAllDependencyAssociationsForTask(entity.getId());
        taskCategoryDao.deleteAllCategoryAssociationsForTask(entity.getId());
        categoryTimeDao.deleteAllCategoryTimesForTask(entity.getId());

        try (var connection = dataSource.getConnection();
             var st = connection.prepareStatement("DELETE FROM TASK WHERE ID = ?")) {
            st.setLong(1, entity.getId());

            int rowsDeleted = st.executeUpdate();
            if (rowsDeleted == 0) {
                throw new DataAccessException("Failed to delete non-existing task: " + entity);
            }
        } catch (SQLException ex) {
            throw new DataAccessException("Failed to delete task " + entity, ex);
        }
    }
}
