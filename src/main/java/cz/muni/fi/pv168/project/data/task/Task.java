package cz.muni.fi.pv168.project.data.task;

import cz.muni.fi.pv168.project.data.IdentifiableEntity;
import cz.muni.fi.pv168.project.data.category.Category;

import java.time.Duration;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.format.FormatStyle;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Objects;

public class Task implements Completable, IdentifiableEntity {

    private Long id;

    private String title;
    private LocalDate dueDate;

    private Duration estimatedTime;
    private String description;
    private TaskStatus taskStatus;

    private List<Category> categories;
    private List<SubTask> subTasks;
    private List<Task> dependencyTasks;

    Task(String title, Long estimatedHours, LocalDate dueDate) {
        this.title = title;
        this.estimatedTime = Duration.ofHours(estimatedHours);
        this.dueDate = LocalDate.from(dueDate);

        categories = new ArrayList<>();
        subTasks = new ArrayList<>();
        dependencyTasks = new ArrayList<>();
        description = "";
        taskStatus = TaskStatus.PLANNED;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getDueDateString() {
        return dueDate.format(DateTimeFormatter.ofLocalizedDate(FormatStyle.LONG));
    }

    public LocalDate getDueDate() {
        return dueDate;
    }

    public void setDueDate(LocalDate dueDate) {
        this.dueDate = dueDate;
    }

    public TaskStatus getTaskStatus() {
        return taskStatus;
    }

    public List<Category> getCategories() {
        return Collections.unmodifiableList(categories);
    }

    public void setCategories(List<Category> categories) {
        this.categories = categories;
    }

    public void removeCategory(Category category) {
        categories.remove(category);
    }

    public void addCategory(Category category) {
        categories.add(category);
    }

    public List<SubTask> getSubTasks() {
        return Collections.unmodifiableList(subTasks);
    }

    public void setSubTasks(List<SubTask> subTasks) {
        this.subTasks = subTasks;
    }

    public void removeSubTask(SubTask subTask) {
        subTasks.remove(subTask);
    }

    public void addSubTask(SubTask subTask) {
        subTasks.add(subTask);
    }

    public List<Task> getDependencyTasks() {
        return Collections.unmodifiableList(dependencyTasks);
    }

    public void setDependencyTasks(List<Task> dependencyTasks) {
        this.dependencyTasks = dependencyTasks;
    }

    public void removeDependencyTask(Task task) {
        dependencyTasks.remove(task);
    }

    public void addDependencyTask(Task task) {
        dependencyTasks.add(task);
    }

    public Long getEstimatedTime() {
        return estimatedTime.toHours();
    }

    public void setEstimatedTime(Duration estimatedTime) {
        this.estimatedTime = estimatedTime;
    }

    @Override
    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public boolean shouldShowUrgency() {
        return isUrgent() && !this.taskStatus.equals(TaskStatus.FINISHED);
    }

    @Override
    public boolean isCompleted() {
        return taskStatus == TaskStatus.FINISHED;
    }

    @Override
    public void toggleIsCompleted() {
        switch (taskStatus) {
            case PLANNED:
                taskStatus = TaskStatus.IN_PROGRESS;
                break;
            case IN_PROGRESS:
                taskStatus = TaskStatus.FINISHED;
                break;
            case FINISHED:
                taskStatus = TaskStatus.PLANNED;
                break;
            default:
        }
    }

    @Override
    public String toString() {
        return this.title;
    }

    public boolean isUrgent() {
        return this.dueDate.compareTo(LocalDate.now()) <= 0;
    }

    public static TaskBuilder builder(String title, Long estimatedTime, LocalDate dueDate) {
        return new TaskBuilder(title, estimatedTime, dueDate);
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public void setTaskStatus(TaskStatus taskStatus) {
        this.taskStatus = taskStatus;
    }

    public static final class TaskBuilder {
        private final Task result;

        private TaskBuilder(String title, Long estimatedTime, LocalDate dueDate) {
            result = new Task(title, estimatedTime, dueDate);
        }

        public TaskBuilder setDescription(String description) {
            result.description = description;
            return this;
        }

        public TaskBuilder setCategories(List<Category> categories) {
            result.categories = new ArrayList<>(categories);
            return this;
        }

        public TaskBuilder setSubTasks(List<SubTask> subTasks) {
            result.subTasks = new ArrayList<>(subTasks);
            return this;
        }

        public TaskBuilder setDependencyTasks(List<Task> dependencyTasks) {
            result.dependencyTasks = new ArrayList<>(dependencyTasks);
            return this;
        }

        public Task build() {
            return result;
        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Task task = (Task) o;
        return this.id != null && this.id.equals(task.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }
}
