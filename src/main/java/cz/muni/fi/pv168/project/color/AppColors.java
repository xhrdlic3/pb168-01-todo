package cz.muni.fi.pv168.project.color;

import java.awt.Color;

public final class AppColors {

    private AppColors() {
    }

    public static final Color RED = new Color(0xE84049);
    public static final Color GREEN = new Color(0x33CC33);

    /**
     * Returns a hex string value of a given color.
     * @param color the color to be converted to hex code string.
     */
    public static String colorToString(Color color) {
        return "#" + Integer.toHexString(color.getRGB()).substring(2).toUpperCase();
    }
}
