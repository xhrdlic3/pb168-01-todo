package cz.muni.fi.pv168.project.ui.dialog;

import cz.muni.fi.pv168.project.color.AppColors;
import cz.muni.fi.pv168.project.data.category.Category;
import cz.muni.fi.pv168.project.ui.component.SimpleColorChooser;
import cz.muni.fi.pv168.project.ui.i18n.I18N;

import javax.swing.JLabel;
import javax.swing.JTextField;
import java.awt.GridBagConstraints;

public class CategoryDialog extends AbstractLaidOutDialog<Category> {

    private Category entity;

    private final JTextField nameField = new JTextField();
    private final SimpleColorChooser colorField = new SimpleColorChooser();
    private final JLabel colorCodeField = new JLabel();

    private static final I18N I18N = new I18N(CategoryDialog.class);

    public CategoryDialog() {
        colorCodeField.setText("#FFFFFF");
        colorField.getSelectionModel().addChangeListener(
                ignored -> colorCodeField.setText(AppColors.colorToString(colorField.getColor())));

        addFields();
    }

    public CategoryDialog(Category initialEntity) {
        this();

        entity = initialEntity;

        nameField.setText(initialEntity.getName());
        colorField.setColor(initialEntity.getColor());
    }

    private void addFields() {
        addLabeledField(I18N.getString("fieldLabelName"), nameField, labelGbc, componentGbc, getRowThenIncrement());
        addLabeledField(I18N.getString("fieldLabelColor"), colorField, labelGbc, componentGbc, getRowThenIncrement());

        var colorCodeGbc = (GridBagConstraints) labelGbc.clone();
        colorCodeGbc.fill = GridBagConstraints.CENTER;
        colorCodeGbc.gridwidth = 3;
        addField(colorCodeField, colorCodeGbc, getRowThenIncrement());
    }

    @Override
    Category getEntity() {
        if (entity == null) {
            entity = new Category(nameField.getText(), colorField.getColor());
        } else {
            entity.setName(nameField.getText());
            entity.setColor(colorField.getColor());
        }

        return entity;
    }
}
