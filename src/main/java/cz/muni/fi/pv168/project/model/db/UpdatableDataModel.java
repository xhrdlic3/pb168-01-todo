package cz.muni.fi.pv168.project.model.db;

import cz.muni.fi.pv168.project.db.interfaces.DataAccessObject;
import cz.muni.fi.pv168.project.ui.dialog.error.ErrorDialog;
import cz.muni.fi.pv168.project.ui.i18n.I18N;

import java.util.logging.Level;
import java.util.logging.Logger;

public class UpdatableDataModel<E> extends AbstractDataModel<E> {

    protected final DataAccessObject<E> dataAccessObject;
    private static final Logger LOG = Logger.getLogger(UpdatableDataModel.class.getName());

    private static final I18N I18N = new I18N(UpdatableDataModel.class);

    public UpdatableDataModel(DataAccessObject<E> dao) {
        this.dataAccessObject = dao;
        updateAll();
    }

    @Override
    public void delete(E entity) {
        dataAccessObject.delete(entity);
    }

    @Override
    public void add(E entity) {
        dataAccessObject.add(entity);
    }

    @Override
    public void update(E entity) {
        dataAccessObject.update(entity);
    }

    public void updateAll() {
        clear();
        try {
            addAll(dataAccessObject.getAll());
        } catch (Exception e) {
            LOG.log(Level.SEVERE, "Error loading all entities from database", e);
            ErrorDialog.show(I18N.getString("queryAllError"), e);
        }
    }
}
