package cz.muni.fi.pv168.project.model;

import javax.swing.AbstractListModel;
import javax.swing.ComboBoxModel;
import javax.swing.DefaultListModel;
import javax.swing.ListModel;
import javax.swing.event.ListDataListener;
import java.util.Collection;
import java.util.Objects;

public class ComboBoxModelAdapter<E> extends AbstractListModel<E> implements ComboBoxModel<E> {

    private final ListModel<E> listModel;
    private Object selectedItem;

    public ComboBoxModelAdapter(ListModel<E> listModel) {
        this.listModel = listModel;
    }

    public static <E> ComboBoxModelAdapter<E> fromCollection(Collection<E> collection) {
        DefaultListModel<E> listModel = new DefaultListModel<>();
        listModel.addAll(collection);
        return new ComboBoxModelAdapter<>(listModel);
    }

    @Override
    public void setSelectedItem(Object item) {
        if (!Objects.equals(item, selectedItem)) {
            selectedItem = item;
            fireContentsChanged(this, -1, -1);
        }
    }

    @Override
    public Object getSelectedItem() {
        return selectedItem;
    }

    @Override
    public int getSize() {
        return listModel.getSize();
    }

    @Override
    public E getElementAt(int i) {
        return listModel.getElementAt(i);
    }

    @Override
    public void addListDataListener(ListDataListener l) {
        super.addListDataListener(l);
        listModel.addListDataListener(l);
    }

    @Override
    public void removeListDataListener(ListDataListener l) {
        super.removeListDataListener(l);
        listModel.removeListDataListener(l);
    }
}
