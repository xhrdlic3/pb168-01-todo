package cz.muni.fi.pv168.project.ui.main;

import cz.muni.fi.pv168.project.db.DaoHolder;
import cz.muni.fi.pv168.project.ui.centering.WindowCenterer;
import cz.muni.fi.pv168.project.ui.main.panel.CardPanel;
import cz.muni.fi.pv168.project.ui.main.tab.TabContainer;
import cz.muni.fi.pv168.project.ui.main.view.CategoryAndStatisticsView;
import cz.muni.fi.pv168.project.ui.main.view.TaskView;
import cz.muni.fi.pv168.project.ui.resources.Icons;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.WindowConstants;
import java.awt.BorderLayout;
import java.awt.Dimension;

public class MainWindow {

    private final CardPanel toolbarPanel;
    private final TabContainer tabContainer;

    public MainWindow(DaoHolder daoHolder) {
        toolbarPanel = new CardPanel();
        tabContainer = initializeTabbedPane(daoHolder);

        JPanel mainPanel = new JPanel(new BorderLayout());
        mainPanel.add(tabContainer.getTabbedPane(), BorderLayout.CENTER);
        mainPanel.add(toolbarPanel.getContainer(), BorderLayout.NORTH);

        JFrame mainFrame = new JFrame();
        mainFrame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        mainFrame.setTitle("to:do");
        mainFrame.setSize(1280, 720);
        mainFrame.setMinimumSize(new Dimension(854, 480));
        mainFrame.setLayout(new BorderLayout());
        mainFrame.add(mainPanel, BorderLayout.CENTER);
        Icons.addTodoIconToWindow(mainFrame);

        WindowCenterer.centerWindowToCurrentScreen(mainFrame);
        mainFrame.setVisible(true);
    }

    private TabContainer initializeTabbedPane(DaoHolder daoHolder) {
        final var container = new TabContainer(toolbarPanel);

        var taskView = new TaskView(daoHolder);
        var categoryView = new CategoryAndStatisticsView(daoHolder);
        container.addTab(taskView);
        container.addTab(categoryView);

        container.getTabbedPane().addChangeListener(e -> {
            changeToolBar();
            taskView.getModel().updateAll();
            categoryView.getModel().updateAll();
        });

        return container;
    }

    private void changeToolBar() {
        toolbarPanel.showComponent(tabContainer.getCurrentTabLabel());
    }
}
