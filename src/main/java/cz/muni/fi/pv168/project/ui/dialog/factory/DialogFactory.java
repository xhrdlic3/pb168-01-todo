package cz.muni.fi.pv168.project.ui.dialog.factory;

import cz.muni.fi.pv168.project.ui.dialog.AbstractLaidOutDialog;

public interface DialogFactory<E> {

    AbstractLaidOutDialog<E> newEditDialog(E entity);

    AbstractLaidOutDialog<E> newAddDialog();
}
