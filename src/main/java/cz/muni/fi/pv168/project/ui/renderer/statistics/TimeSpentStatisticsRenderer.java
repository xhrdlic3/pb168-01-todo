package cz.muni.fi.pv168.project.ui.renderer.statistics;

import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JPanel;

public class TimeSpentStatisticsRenderer extends AbstractStatisticsRenderer<String> {
    public TimeSpentStatisticsRenderer() {
        super(String.class);
    }

    @Override
    protected JComponent modifyComponent(String value) {
        final var panel = new JPanel();
        panel.add(new JLabel(value));
        return panel;
    }
}
