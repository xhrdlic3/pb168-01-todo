package cz.muni.fi.pv168.project.ui.renderer.list;

import cz.muni.fi.pv168.project.ui.main.panel.CircleColorPanel;

import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.ListCellRenderer;
import java.awt.Color;
import java.awt.Component;
import java.util.function.Function;

public class InnerColoredListRenderer<T> implements ListCellRenderer<T> {

    private final InnerListRenderer<T> listRenderer;
    private final Function<T, Color> colorGetter;

    public InnerColoredListRenderer(Function<T, String> stringGetter, Function<T, Color> colorGetter, int fontStyle) {
        listRenderer = new InnerListRenderer<>(stringGetter, fontStyle);
        this.colorGetter = colorGetter;
    }

    @Override
    public Component getListCellRendererComponent(JList<? extends T> jList, T t, int i, boolean b, boolean b1) {
        JPanel panel = (JPanel) listRenderer.getListCellRendererComponent(jList, t, i, b, b1);
        panel.add(new CircleColorPanel(colorGetter.apply(t)));
        return panel;
    }
}
