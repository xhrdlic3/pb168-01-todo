package cz.muni.fi.pv168.project.data.category;

import java.time.Duration;
import java.time.LocalDate;
import java.util.Objects;

public class CategoryTime {

    private Long id;

    private final Long taskId;
    private final LocalDate completionDate;
    private Duration timeSpent;

    public CategoryTime(LocalDate completionDate, Long hoursSpent, Long taskId) {
        this.completionDate = completionDate;
        this.timeSpent = Duration.ofHours(hoursSpent);
        this.taskId = taskId;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public LocalDate getCompletionDate() {
        return completionDate;
    }

    public Long getTaskId() {
        return taskId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        CategoryTime that = (CategoryTime) o;
        return this.id != null && this.id.equals(that.id);
    }

    public void setHoursSpent(Long hoursSpent) {
        this.timeSpent = Duration.ofHours(hoursSpent);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }

    public Duration getTimeSpent() {
        return timeSpent;
    }
}
