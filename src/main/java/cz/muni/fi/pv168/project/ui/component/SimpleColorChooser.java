package cz.muni.fi.pv168.project.ui.component;

import cz.muni.fi.pv168.project.Main;

import javax.swing.JColorChooser;
import javax.swing.JComponent;
import javax.swing.JPanel;
import javax.swing.colorchooser.AbstractColorChooserPanel;

public class SimpleColorChooser extends JColorChooser {

    public SimpleColorChooser() {
        // Hides preview panel
        this.setPreviewPanel(new JPanel());

        // Discards all but HSV chooser panel
        for (var panel : this.getChooserPanels()) {
            if (panel.getDisplayName().equals("HSV")) {
                this.setChooserPanels(new AbstractColorChooserPanel[]{panel});
                break;
            }
        }

        // Hides WinOS specific color sliders
        if (Main.isOsWindows()) {
            var item = (JComponent) this.getComponents()[0];
            while (!item.getClass().toString().equals("class javax.swing.colorchooser.ColorChooserPanel")) {
                item = (JComponent) item.getComponents()[0];
            }
            for (var component : item.getComponents()) {
                component.setVisible(component.getClass().toString()
                        .equals("class javax.swing.colorchooser.DiagramComponent"));
            }
        }
    }
}
