package cz.muni.fi.pv168.project.ui.main.panel.listholder;

import cz.muni.fi.pv168.project.data.task.Task;
import cz.muni.fi.pv168.project.db.dependency.DependencyDao;
import cz.muni.fi.pv168.project.db.task.TaskDao;
import cz.muni.fi.pv168.project.model.db.TaskDependencyModel;
import cz.muni.fi.pv168.project.ui.action.AddAction;
import cz.muni.fi.pv168.project.ui.action.DeleteAction;
import cz.muni.fi.pv168.project.ui.action.task.GoToAction;
import cz.muni.fi.pv168.project.ui.dialog.factory.DependencyTaskDialogFactory;
import cz.muni.fi.pv168.project.ui.dialog.factory.DialogFactory;
import cz.muni.fi.pv168.project.ui.i18n.I18N;
import cz.muni.fi.pv168.project.ui.popup.PopupMenuFactory;
import cz.muni.fi.pv168.project.ui.renderer.list.TaskStatusListRenderer;
import cz.muni.fi.pv168.project.ui.resources.Icons;

import javax.swing.JList;
import java.awt.Font;
import java.util.Optional;

public class DependencyHolderPanel extends AbstractHolderPanel<Task> {

    private final DependencyTaskDialogFactory factory;

    private static final I18N I18N = new I18N(DependencyHolderPanel.class);

    public DependencyHolderPanel(JList<Task> superiorTaskList, DependencyDao dependencyDao, TaskDao taskDao) {
        super(
                new TaskDependencyModel(dependencyDao), I18N.getString("panelName"),
                new TaskStatusListRenderer<>(Task::getTitle, Task::getTaskStatus, Task::isUrgent, Font.PLAIN),
                Optional.of(Task::isCompleted)
        );

        factory = new DependencyTaskDialogFactory(dependencyDao, taskDao);
        PopupMenuFactory.builder()
                .addMenuItem(new GoToAction(this, superiorTaskList, I18N.getString("goToAction")))
                .addSeparator()
                .addMenuItem(new AddAction<>(this, Icons.ADD_TASK_ICON, I18N.getString("addAction")).addUpdatablePanel(this))
                .addMenuItem(new DeleteAction<>(this, Icons.REMOVE_TASK_BIN_ICON, I18N.getString("removeAction")).addUpdatablePanel(this))
                .buildFor(this.getList());
    }

    @Override
    public DialogFactory<Task> getDialogFactory() {
        factory.setSourceEntity(getModel().getParentTask());
        return factory;
    }
}
