package cz.muni.fi.pv168.project.ui.main.panel;

import cz.muni.fi.pv168.project.color.AppColors;

import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import java.awt.Color;
import java.awt.FlowLayout;
import java.util.function.Predicate;

public class CounterPanel<T> extends JPanel {

    private final JLabel completedSubTasks;
    private final JLabel totalSubTasks;
    private final JList<T> jListParent;
    private final Predicate<T> shouldBeCounted;

    public CounterPanel(JList<T> jListParentReference, Predicate<T> shouldBeCounted) {
        super(new FlowLayout(FlowLayout.RIGHT));
        completedSubTasks = new JLabel("0");
        totalSubTasks = new JLabel("0");
        jListParent = jListParentReference;
        this.shouldBeCounted = shouldBeCounted;

        this.add(completedSubTasks);
        this.add(new JLabel("/"));
        this.add(totalSubTasks);
    }

    public void updateCounter() {
        int total = jListParent.getModel().getSize();
        int completed = 0;
        for (int i = 0; i < total; i++) {
            if (shouldBeCounted.test(jListParent.getModel().getElementAt(i))) {
                completed++;
            }
        }
        Color color = completed == total ? AppColors.GREEN : AppColors.RED;

        completedSubTasks.setText(String.valueOf(completed));
        completedSubTasks.setForeground(color);

        totalSubTasks.setText(String.valueOf(total));
        totalSubTasks.setForeground(color);
    }
}
