package cz.muni.fi.pv168.project.model;

import javax.swing.ComboBoxModel;
import javax.swing.ListModel;
import javax.swing.event.ListDataEvent;
import javax.swing.event.ListDataListener;
import java.util.IdentityHashMap;
import java.util.Map;

public final class NullValueModelDecorator {

    private NullValueModelDecorator() {
    }

    public static <E> ComboBoxModel<E> addNullValue(ComboBoxModel<E> decoratedModel) {
        return new ComboBoxModelDecorator<>(decoratedModel);
    }

    public static <E> ListModel<E> addNullValue(ListModel<E> decoratedModel) {
        return new ListModelDecorator<>(decoratedModel);
    }

    @SuppressWarnings("checkstyle:FinalClass")
    private static class ListModelDecorator<E, M extends ListModel<E>> implements ListModel<E> {

        private final M decoratedModel;
        private final Map<ListDataListener, TransposingListener> listeners = new IdentityHashMap<>();

        private ListModelDecorator(M decoratedModel) {
            this.decoratedModel = decoratedModel;
        }

        @Override
        public int getSize() {
            return decoratedModel.getSize() + 1;
        }

        @Override
        public E getElementAt(int index) {
            if (index == 0) {
                return null;
            } else {
                return decoratedModel.getElementAt(index - 1);
            }
        }

        @Override
        public void addListDataListener(ListDataListener listener) {
            var transposingListener = new TransposingListener(listener);
            listeners.put(listener, transposingListener);
            decoratedModel.addListDataListener(transposingListener);
        }

        @Override
        public void removeListDataListener(ListDataListener listener) {
            TransposingListener transposingListener = listeners.remove(listener);
            if (transposingListener != null) {
                decoratedModel.removeListDataListener(transposingListener);
            }
        }

    }

    private static final class ComboBoxModelDecorator<E> extends ListModelDecorator<E, ComboBoxModel<E>> implements ComboBoxModel<E> {

        private ComboBoxModelDecorator(ComboBoxModel<E> decoratedModel) {
            super(decoratedModel);
        }

        @Override
        public Object getSelectedItem() {
            return super.decoratedModel.getSelectedItem();
        }

        @Override
        public void setSelectedItem(Object anItem) {
            super.decoratedModel.setSelectedItem(anItem);
        }

    }

    private static final class TransposingListener implements ListDataListener {

        private final ListDataListener delegate;

        private TransposingListener(ListDataListener delegate) {
            this.delegate = delegate;
        }

        @Override
        public void intervalAdded(ListDataEvent event) {
            delegate.intervalAdded(transposeIndexes(event));
        }

        @Override
        public void intervalRemoved(ListDataEvent event) {
            delegate.intervalRemoved(transposeIndexes(event));
        }

        @Override
        public void contentsChanged(ListDataEvent event) {
            delegate.contentsChanged(transposeIndexes(event));
        }

        private ListDataEvent transposeIndexes(ListDataEvent event) {
            return new ListDataEvent(
                    event.getSource(),
                    event.getType(),
                    transposeIndex(event.getIndex0()),
                    transposeIndex(event.getIndex1())
            );
        }

        private int transposeIndex(int index) {
            return index >= 0 ? index + 1 : index;
        }
    }
}
