package cz.muni.fi.pv168.project.ui.action;

import cz.muni.fi.pv168.project.data.task.Task;
import cz.muni.fi.pv168.project.ui.dialog.factory.DialogFactory;
import cz.muni.fi.pv168.project.ui.main.tab.ListHolder;

import javax.swing.JList;
import java.util.ArrayList;
import java.util.List;
import java.util.function.IntPredicate;
import java.util.function.Supplier;

public abstract class AbstractAction<T> extends javax.swing.AbstractAction {

    private final Supplier<Task> taskSupplier;
    private IntPredicate enabledCondition;
    protected final ListHolder<T> listHolder;

    private final List<UpdatablePanel> updatablePanels = new ArrayList<>();

    public AbstractAction(ListHolder<T> listHolder, IntPredicate enabledCondition, Supplier<Task> taskSupplier) {
        this.enabledCondition = enabledCondition;
        this.taskSupplier = taskSupplier;
        this.listHolder = listHolder;
        listHolder.getList().getSelectionModel().addListSelectionListener(e -> updateEnabledStatus());
        updateEnabledStatus();
    }

    public AbstractAction(ListHolder<T> listHolder, IntPredicate enabledCondition) {
        this(listHolder, enabledCondition, () -> null);
    }

    private void updateEnabledStatus() {
        setEnabled(enabledCondition.test(listHolder.getList().getSelectionModel().getSelectedItemsCount()));
    }

    protected JList<T> getList() {
        return listHolder.getList();
    }

    protected DialogFactory<T> getDialogFactory() {
        return listHolder.getDialogFactory();
    }

    public AbstractAction<T> addUpdatablePanel(UpdatablePanel updatablePanel) {
        updatablePanels.add(updatablePanel);

        return this;
    }

    public AbstractAction<T> addEnabledCondition(IntPredicate anotherCondition) {
        this.enabledCondition = enabledCondition.and(anotherCondition);

        return this;
    }

    public void triggerUpdate() {
        var value = taskSupplier.get();
        updatablePanels.forEach(updatablePanel -> updatablePanel.updateFrom(value));
    }
}
