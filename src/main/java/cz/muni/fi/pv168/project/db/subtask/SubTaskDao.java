package cz.muni.fi.pv168.project.db.subtask;

import cz.muni.fi.pv168.project.data.task.SubTask;
import cz.muni.fi.pv168.project.db.DataAccessException;

import javax.sql.DataSource;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

public class SubTaskDao {

    private final DataSource dataSource;

    public SubTaskDao(DataSource dataSource) {
        this.dataSource = dataSource;
    }

    public List<SubTask> getAllSubTasksForTask(Long taskId) {
        if (taskId == null) {
            throw new IllegalArgumentException("Cannot look for subtasks of null ID parent");
        }

        try (var connection = dataSource.getConnection();
             var st = connection.prepareStatement("SELECT ID, TITLE, IS_COMPLETED FROM SUBTASK WHERE TASK_ID = ?")) {
            st.setLong(1, taskId);

            List<SubTask> subTasks = new ArrayList<>();
            try (var rs = st.executeQuery()) {
                while (rs.next()) {
                    subTasks.add(toEntity(rs));
                }

                return subTasks;
            }
        } catch (SQLException ex) {
            throw new DataAccessException("Failed to load subtasks with task id " + taskId, ex);
        }
    }

    public void deleteAllSubTasksForTask(Long taskId) {
        var subTasks = getAllSubTasksForTask(taskId);
        for (var subTask : subTasks) {
            delete(subTask);
        }
    }

    public void addSubTaskForTask(Long taskId, SubTask subTask) {
        if (subTask.getId() != null) {
            throw new IllegalArgumentException("Subtask already has ID: " + subTask);
        }

        try (var connection = dataSource.getConnection();
             var st = connection.prepareStatement(
                     "INSERT INTO SUBTASK (TASK_ID, TITLE, IS_COMPLETED) VALUES (?, ?, ?)",
                     Statement.RETURN_GENERATED_KEYS)) {
            st.setLong(1, taskId);
            st.setString(2, subTask.getTitle());
            st.setBoolean(3, subTask.isCompleted());

            st.executeUpdate();

            try (var rs = st.getGeneratedKeys()) {
                if (rs.getMetaData().getColumnCount() != 1) {
                    throw new DataAccessException("Failed to fetch generated key: compound key returned for subtask: " + subTask);
                }
                if (rs.next()) {
                    subTask.setId(rs.getLong(1));
                } else {
                    throw new DataAccessException("Failed to fetch generated key: no key returned for subtask: " + subTask);
                }
                if (rs.next()) {
                    throw new DataAccessException("Failed to fetch generated key: multiple keys returned for subtask: " + subTask);
                }
            }
        } catch (SQLException ex) {
            throw new DataAccessException("Failed to store subtask " + subTask, ex);
        }
    }

    public void update(SubTask subTask) {
        if (subTask.getId() == null) {
            throw new IllegalArgumentException("Subtask has null ID: " + subTask);
        }

        try (var connection = dataSource.getConnection();
             var st = connection.prepareStatement(
                     "UPDATE SUBTASK SET TITLE = ?, IS_COMPLETED = ? WHERE ID = ?")) {
            st.setString(1, subTask.getTitle());
            st.setBoolean(2, subTask.isCompleted());
            st.setLong(3, subTask.getId());

            int rowsUpdated = st.executeUpdate();
            if (rowsUpdated == 0) {
                throw new DataAccessException("Failed to update non-existing subtask: " + subTask);
            }
        } catch (SQLException ex) {
            throw new DataAccessException("Failed to update subtask " + subTask, ex);
        }
    }

    public void delete(SubTask subTask) {
        if (subTask.getId() == null) {
            throw new IllegalArgumentException("Subtask has null ID: " + subTask);
        }

        try (var connection = dataSource.getConnection();
             var st = connection.prepareStatement("DELETE FROM SUBTASK WHERE ID = ?")) {
            st.setLong(1, subTask.getId());

            int rowsDeleted = st.executeUpdate();
            if (rowsDeleted == 0) {
                throw new DataAccessException("Failed to delete non-existing subtask: " + subTask);
            }
        } catch (SQLException ex) {
            throw new DataAccessException("Failed to delete subtask " + subTask, ex);
        }
    }

    public SubTask findById(Long id) {
        if (id == null) {
            throw new IllegalArgumentException("Cannot look for subtask of null ID");
        }

        try (var connection = dataSource.getConnection();
             var st = connection.prepareStatement("SELECT ID, TITLE, IS_COMPLETED FROM SUBTASK WHERE ID = ?")) {
            st.setLong(1, id);

            try (var rs = st.executeQuery()) {
                if (rs.next()) {
                    var task = toEntity(rs);
                    if (rs.next()) {
                        throw new DataAccessException("Multiple subtasks with id " + id + " found");
                    }
                    return task;
                }
                return null;
            }
        } catch (SQLException ex) {
            throw new DataAccessException("Failed to load subtask with id " + id, ex);
        }
    }

    private SubTask toEntity(ResultSet rs) throws SQLException {
        var subTask = new SubTask(
            rs.getString("TITLE"),
            rs.getBoolean("IS_COMPLETED")
        );
        subTask.setId(rs.getLong("ID"));
        return subTask;
    }
}
