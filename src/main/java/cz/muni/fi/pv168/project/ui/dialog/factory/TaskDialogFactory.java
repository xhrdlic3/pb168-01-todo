package cz.muni.fi.pv168.project.ui.dialog.factory;

import cz.muni.fi.pv168.project.data.task.Task;
import cz.muni.fi.pv168.project.ui.dialog.AbstractLaidOutDialog;
import cz.muni.fi.pv168.project.ui.dialog.TaskDialog;

public class TaskDialogFactory implements DialogFactory<Task> {

    @Override
    public AbstractLaidOutDialog<Task> newEditDialog(Task entity) {
        return new TaskDialog(entity);
    }

    @Override
    public AbstractLaidOutDialog<Task> newAddDialog() {
        return new TaskDialog();
    }
}
