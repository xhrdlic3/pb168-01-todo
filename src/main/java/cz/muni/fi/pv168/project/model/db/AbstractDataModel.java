package cz.muni.fi.pv168.project.model.db;

import cz.muni.fi.pv168.project.ui.dialog.error.ErrorDialog;
import cz.muni.fi.pv168.project.ui.i18n.I18N;

import javax.swing.DefaultListModel;
import java.util.logging.Level;
import java.util.logging.Logger;

public abstract class AbstractDataModel<E> extends DefaultListModel<E> {

    private static final I18N I18N = new I18N(AbstractDataModel.class);
    private static final Logger LOG = Logger.getLogger(AbstractDataModel.class.getName());

    @Override
    public void setElementAt(E element, int index) {
        try {
            update(element);
            super.setElementAt(element, index);
        } catch (Exception e) {
            LOG.log(Level.SEVERE, "Error when updating entity " + element, e);
            ErrorDialog.show(I18N.getString("editingEntError") + " " + element, e);
        }
    }

    @Override
    public void addElement(E element) {
        try {
            add(element);
            super.addElement(element);
        } catch (Exception e) {
            LOG.log(Level.SEVERE, "Error when adding entity " + element, e);
            ErrorDialog.show(I18N.getString("addingEntError") + " " + element, e);
        }
    }

    @Override
    @SuppressWarnings("unchecked") // safe
    public boolean removeElement(Object obj) {
        try {
            delete((E) obj);
            return super.removeElement(obj);
        } catch (Exception e) {
            LOG.log(Level.SEVERE, "Error when removing entity " + obj, e);
            ErrorDialog.show(I18N.getString("deletingEntError") + " " + obj, e);
        }
        return false;
    }

    protected abstract void update(E element);

    protected abstract void delete(E obj);

    protected abstract void add(E element);
}
