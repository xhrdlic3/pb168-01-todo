package cz.muni.fi.pv168.project.data.category;

import cz.muni.fi.pv168.project.data.IdentifiableEntity;

import java.awt.*;
import java.time.Duration;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class Category implements IdentifiableEntity {

    private Long id;
    private String name;
    private Color color;
    private List<CategoryTime> timeSpents;

    public Category(String name, Color color) {
        this.timeSpents = new ArrayList<>();
        this.name = name;
        this.color = color;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Color getColor() {
        return color;
    }

    public void setColor(Color color) {
        this.color = color;
    }

    @Override
    public String toString() {
        return this.name;
    }

    public Duration getTotalDuration(LocalDate fromDate, LocalDate toDate) {
        return this.timeSpents
                .stream()
                .filter(categoryTime -> categoryTime.getCompletionDate().compareTo(fromDate) >= 0)
                .filter(categoryTime -> categoryTime.getCompletionDate().compareTo(toDate) <= 0)
                .map(CategoryTime::getTimeSpent)
                .reduce(Duration.ZERO, Duration::plus);
    }

    public void setTimeSpents(List<CategoryTime> timeSpents) {
        this.timeSpents = timeSpents;
    }

    public List<CategoryTime> getTimeSpents() {
        return timeSpents;
    }

    public void addTime(Long hours, Long taskId) {
        timeSpents.add(new CategoryTime(LocalDate.now(), hours, taskId));
    }

    public void subtractTime(Long taskId) {
        timeSpents.removeIf(categoryTime -> categoryTime.getTaskId().equals(taskId));
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Category category = (Category) o;
        return this.id != null && this.id.equals(category.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }
}
