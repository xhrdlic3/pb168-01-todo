package cz.muni.fi.pv168.project.model.db;

import cz.muni.fi.pv168.project.data.category.Category;
import cz.muni.fi.pv168.project.data.task.Task;
import cz.muni.fi.pv168.project.db.category.CategoryDao;
import cz.muni.fi.pv168.project.db.taskcategory.TaskCategoryDao;

public class TaskCategoryModel extends AbstractTaskPropertyListDataModel<Category> {

    private final TaskCategoryDao dataAccessObject;
    private final CategoryDao parentDao;

    public TaskCategoryModel(TaskCategoryDao dao, CategoryDao parentDao) {
        super(Task::getCategories);
        this.dataAccessObject = dao;
        this.parentDao = parentDao;
    }

    @Override
    public void delete(Category entity) {
        dataAccessObject.deleteCategoryAssociationFor(parentTask.getId(), entity.getId());
        parentTask.removeCategory(entity);
    }

    @Override
    public void add(Category entity) {
        dataAccessObject.addCategoryAssociationFor(parentTask.getId(), entity.getId());
        parentTask.addCategory(entity);
    }

    @Override
    public void update(Category entity) {
        parentDao.update(entity);
    }
}
