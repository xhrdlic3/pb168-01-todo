package cz.muni.fi.pv168.project.ui.action;

import cz.muni.fi.pv168.project.data.task.Task;
import org.jetbrains.annotations.Nullable;

public interface UpdatablePanel {

    void updateFrom(@Nullable Task task);
}
