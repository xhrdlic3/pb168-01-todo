package cz.muni.fi.pv168.project.ui.dialog;

import cz.muni.fi.pv168.project.model.ComboBoxModelAdapter;
import cz.muni.fi.pv168.project.ui.i18n.I18N;

import javax.swing.JComboBox;
import java.util.Collection;

public class ComboBoxDialog<T> extends AbstractLaidOutDialog<T> {

    private final JComboBox<T> comboBox;
    private static final I18N I18N = new I18N(ComboBoxDialog.class);

    public ComboBoxDialog(Collection<T> entities) {
        comboBox = new JComboBox<>(ComboBoxModelAdapter.fromCollection(entities));
        addLabeledField(I18N.getString("fieldLabelSelect"), comboBox, labelGbc, middleComponent, getRow());

        // gray out button when nothing is initially selected
        getOkButton().setEnabled(false);
        comboBox.addActionListener(e -> getOkButton().setEnabled(true));
    }

    @SuppressWarnings("unchecked") // safe
    @Override
    T getEntity() {
        return (T) comboBox.getSelectedItem();
    }
}
