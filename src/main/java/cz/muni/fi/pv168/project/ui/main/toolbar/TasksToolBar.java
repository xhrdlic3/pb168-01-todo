package cz.muni.fi.pv168.project.ui.main.toolbar;

import cz.muni.fi.pv168.project.data.task.TaskStatus;
import cz.muni.fi.pv168.project.model.NullValueModelDecorator;
import cz.muni.fi.pv168.project.ui.action.TabActions;
import cz.muni.fi.pv168.project.ui.i18n.I18N;

import javax.swing.Action;
import javax.swing.Box;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JToolBar;
import java.awt.event.ActionListener;

public class TasksToolBar {

    private final JToolBar toolBar;

    private final JCheckBox isJustToday;
    private final JComboBox<TaskStatus> taskTypes;

    private static final I18N I18N = new I18N(TasksToolBar.class);

    public TasksToolBar(TabActions tabActions) {
        toolBar = new JToolBar(JToolBar.HORIZONTAL);
        toolBar.setFloatable(false);

        isJustToday = new JCheckBox(I18N.getString("filter"), false);
        isJustToday.setToolTipText(I18N.getString("filterTooltip"));
        var model = NullValueModelDecorator.addNullValue(new DefaultComboBoxModel<>(TaskStatus.values()));
        taskTypes = new JComboBox<>(model);
        taskTypes.setSelectedItem(null);

        for (Action action : tabActions.getLeftSideActions()) {
            toolBar.add(action);
        }

        toolBar.add(Box.createHorizontalGlue());
        toolBar.add(isJustToday);
        toolBar.add(Box.createHorizontalStrut(5));
        toolBar.add(taskTypes);
    }

    public void addTodayActionListener(ActionListener l) {
        isJustToday.addActionListener(l);
    }

    public void addStatusActionListener(ActionListener l) {
        taskTypes.addActionListener(l);
    }

    public Object getSelectedStatus() {
        return taskTypes.getSelectedItem();
    }

    public boolean isUrgentSelected() {
        return isJustToday.isSelected();
    }

    public JToolBar getToolBar() {
        return toolBar;
    }
}
