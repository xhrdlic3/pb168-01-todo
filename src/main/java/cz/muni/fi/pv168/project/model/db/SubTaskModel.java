package cz.muni.fi.pv168.project.model.db;

import cz.muni.fi.pv168.project.data.task.SubTask;
import cz.muni.fi.pv168.project.data.task.Task;
import cz.muni.fi.pv168.project.db.subtask.SubTaskDao;

public class SubTaskModel extends AbstractTaskPropertyListDataModel<SubTask> {

    private final SubTaskDao dataAccessObject;

    public SubTaskModel(SubTaskDao dao) {
        super(Task::getSubTasks);
        this.dataAccessObject = dao;
    }

    @Override
    public void delete(SubTask entity) {
        dataAccessObject.delete(entity);
        parentTask.removeSubTask(entity);
    }

    @Override
    public void add(SubTask entity) {
        dataAccessObject.addSubTaskForTask(parentTask.getId(), entity);
        parentTask.addSubTask(entity);
    }

    @Override
    public void update(SubTask entity) {
        dataAccessObject.update(entity);
    }
}
