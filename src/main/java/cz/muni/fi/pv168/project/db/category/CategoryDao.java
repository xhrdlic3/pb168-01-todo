package cz.muni.fi.pv168.project.db.category;

import cz.muni.fi.pv168.project.data.category.Category;
import cz.muni.fi.pv168.project.db.DataAccessException;
import cz.muni.fi.pv168.project.db.categorytime.CategoryTimeDao;
import cz.muni.fi.pv168.project.db.interfaces.DataAccessObject;

import javax.sql.DataSource;
import java.awt.Color;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class CategoryDao implements DataAccessObject<Category> {

    private final DataSource dataSource;
    private final CategoryTimeDao categoryTimeDao;

    public CategoryDao(DataSource dataSource, CategoryTimeDao categoryTimeDao) {
        this.dataSource = dataSource;
        this.categoryTimeDao = categoryTimeDao;
    }

    @Override
    public void add(Category entity) throws DataAccessException {
        if (entity.getId() != null) {
            throw new IllegalArgumentException("Category already has ID: " + entity);
        }

        try (var connection = dataSource.getConnection();
             var st = connection.prepareStatement(
                     "INSERT INTO CATEGORY (TITLE, COLOR) VALUES (?, ?)",
                     Statement.RETURN_GENERATED_KEYS)) {
            st.setString(1, entity.getName());
            st.setInt(2, entity.getColor().getRGB());

            st.executeUpdate();

            try (var rs = st.getGeneratedKeys()) {
                if (rs.getMetaData().getColumnCount() != 1) {
                    throw new DataAccessException("Failed to fetch generated key: compound key returned for task: " + entity);
                }
                if (rs.next()) {
                    entity.setId(rs.getLong(1));
                } else {
                    throw new DataAccessException("Failed to fetch generated key: no key returned for task: " + entity);
                }
                if (rs.next()) {
                    throw new DataAccessException("Failed to fetch generated key: multiple keys returned for task: " + entity);
                }
            }
        } catch (SQLException ex) {
            throw new DataAccessException("Failed to store task " + entity, ex);
        }
    }

    @Override
    public Collection<Category> getAll() throws DataAccessException {
        try (var connection = dataSource.getConnection();
             var st = connection.prepareStatement("SELECT ID, TITLE, COLOR FROM CATEGORY")) {
            List<Category> categories = new ArrayList<>();

            try (var rs = st.executeQuery()) {
                while (rs.next()) {
                    categories.add(toEntity(rs));
                }
            }

            return categories;
        } catch (SQLException ex) {
            throw new DataAccessException("Failed to load all categories", ex);
        }
    }

    @Override
    public void update(Category entity) throws DataAccessException {
        if (entity.getId() == null) {
            throw new IllegalArgumentException("Category has null ID");
        }

        try (var connection = dataSource.getConnection();
             var st = connection.prepareStatement(
                     "UPDATE CATEGORY SET TITLE = ?, COLOR = ? WHERE ID = ?")) {
            st.setString(1, entity.getName());
            st.setInt(2, entity.getColor().getRGB());
            st.setLong(3, entity.getId());

            int rowsUpdated = st.executeUpdate();
            if (rowsUpdated == 0) {
                throw new DataAccessException("Failed to update non-existing category: " + entity);
            }

            // cleanup previous
            categoryTimeDao.deleteAllCategoryTimesForCategory(entity.getId());
            // add current
            for (var categoryTime : entity.getTimeSpents()) {
                categoryTime.setId(null);
                categoryTimeDao.addCategoryTimeFor(entity.getId(), categoryTime);
            }
        } catch (SQLException ex) {
            throw new DataAccessException("Failed to update category " + entity, ex);
        }
    }

    @Override
    public void delete(Category entity) throws DataAccessException {
        if (entity.getId() == null) {
            throw new IllegalArgumentException("Category has null ID: " + entity);
        }

        categoryTimeDao.deleteAllCategoryTimesForCategory(entity.getId());

        try (var connection = dataSource.getConnection();
             var st = connection.prepareStatement("DELETE FROM CATEGORY WHERE ID = ?")) {
            st.setLong(1, entity.getId());

            int rowsDeleted = st.executeUpdate();
            if (rowsDeleted == 0) {
                throw new DataAccessException("Failed to delete non-existing category: " + entity);
            }
        } catch (SQLException ex) {
            throw new DataAccessException("Failed to delete category " + entity, ex);
        }
    }

    @Override
    public Category fetch(Long id) {
        try (var connection = dataSource.getConnection();
             var st = connection.prepareStatement(
                     "SELECT ID, TITLE, COLOR FROM CATEGORY WHERE ID = ?")) {
            st.setLong(1, id);

            try (var rs = st.executeQuery()) {
                if (rs.next()) {
                    var task = toEntity(rs);

                    if (rs.next()) {
                        throw new DataAccessException("Multiple categories with id " + id + " found");
                    }

                    return task;
                }
                return null;
            }
        } catch (SQLException ex) {
            throw new DataAccessException("Failed to load category with id " + id, ex);
        }
    }

    private Category toEntity(ResultSet rs) throws SQLException {
        var id = rs.getLong("ID");
        var category = new Category(
                rs.getString("TITLE"),
                new Color(rs.getInt("COLOR"))
        );

        category.setId(id);
        category.setTimeSpents(categoryTimeDao.getAllCategoryTimesForCategory(id));

        return category;
    }
}
